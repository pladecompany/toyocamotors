<?php
  include_once("modelo/Citas.php");

  if(isset($_GET['aprobar'])){
    $idc = $_GET['aprobar'];
    $est = 1;
    $cita = new Cita();
    if($cita->cambiarEstatus($est, $idc)){
      $err = "¡Cita aprobada correctamente!";
      echo "<script>window.location ='?op=solicitudes&info&msj=$err';</script>";
    }else{
      $err = "No se pudo aprobar la cita!";
      echo "<script>window.location ='?op=solicitudes&err&msj=$err';</script>";
    }
  }else if(isset($_GET['rechazar'])){
    $idc = $_GET['rechazar'];
    $est = -2;
    $cita = new Cita();
    if($cita->cambiarEstatus($est, $idc)){
      $err = "¡Cita rechazada correctamente!";
      echo "<script>window.location ='?op=solicitudes&info&msj=$err';</script>";
    }else{
      $err = "No se pudo rechazar la cita!";
      echo "<script>window.location ='?op=solicitudes&err&msj=$err';</script>";
    }
  }else if(isset($_GET['finalizar'])){
    $idc = $_GET['finalizar'];
    $est = 2;
    $cita = new Cita();
    if($cita->cambiarEstatus($est, $idc)){
      $err = "¡Cita finalizada correctamente!";
      echo "<script>window.location ='?op=solicitudes&info&msj=$err';</script>";
    }else{
      $err = "No se pudo finalizar la cita!";
      echo "<script>window.location ='?op=solicitudes&err&msj=$err';</script>";
    }
  }else if(isset($_GET['cancelar'])){
    $idc = $_GET['cancelar'];
    $est = -1;
    $cita = new Cita();
    if($cita->cambiarEstatus($est, $idc)){
      $err = "¡Cita cancelada correctamente!";
      echo "<script>window.location ='?op=solicitudes&info&msj=$err';</script>";
    }else{
      $err = "No se pudo cancelar la cita!";
      echo "<script>window.location ='?op=solicitudes&err&msj=$err';</script>";
    }
  }

?>

<div class="container mt-5">
	<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Solicitudes</h4>
	</div>

	<div class="card-body">
              <?php include_once("mensajes.php");?>
        <form action="" method="GET">
          <input type="hidden" name="op" value="solicitudes">
          <div class="row">
            <?php
              if(isset($_GET['fec1']) && isset($_GET['fec2'])){
                $primero = $_GET['fec1'];
                $ultimo = $_GET['fec2'];
              }else{
                $fa = date('Y-m-d');
                $primero = date("Y-m-d", strtotime($fa. "- 4 days"));
                $a_date = date('Y-m-d');
                //$ultimo = date("Y-m-t", strtotime($a_date));
                $ultimo = date('Y-m-d');
              }
            ?>
            <div class="col-md-3">

              <label>Desde: </label>
              <input type="date" name="fec1" class="form-control" value="<?php echo (!isset($_GET['fec1']))?$primero:$_GET['fec1'];?>">
            </div>
            <div class="col-md-3">
              <label>Hasta: </label>
              <input type="date" name="fec2" class="form-control" value="<?php echo (!isset($_GET['fec2']))?$ultimo:$_GET['fec2']?>">
            </div>
            <div class="col-md-3">
              <input type="submit" name="btf" class="btn btn-danger" value="filtrar">
            </div>
          </div>
        </form>
        <hr>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Enviado</th>
						<th>Asesor</th>
						<th>Cliente</th>
						<th>Vehículo</th>
						<th>Modelo</th>
						<th>Cita</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
	              <?php
	                $noti = new Cita();
	                $r = $noti->citasGenerales($primero, $ultimo);
	                $i=0;
	                while($ff = $r->fetch_assoc()){
	                  $i++;
	                  echo "<tr>";
	                  echo "  <td>" . $i . "</td>";
                      echo "  <td class='momento1'>" . $ff['fec_env'] . "</td>";
	                  echo "  <td>" . $ff['cod_age'] . "</td>";
	                  echo "  <td>" . $ff['ced_usu'] . "</td>";
	                  echo "  <td>" . $ff['placa'] . "</td>";
	                  echo "  <td>" . $ff['modelo'] . "</td>";
                      echo "  <td class='momento1' >" . $ff['fecha'] . "</td>";
                      echo "  <td id='estado_".$ff['idc']."' style='background: ".$noti->estatus($ff['estatus'])["color"].";color:#fff;'>" . $noti->estatus($ff['estatus'])["txt"] . "</td>";
                      echo "<td class='text-center'>";
                      echo "<a href='#' title='Ver detalles' class='bt_detalle' id='".$ff['idc']."'><i class='mr-2 fa fa-eye'></i></a>";
                      echo "</td>";
	                  echo "</tr>";
	                }
	              ?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
</div>

<div id="md-cita" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h5>Solicitud de cita</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
                  <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
                      <div class="row">
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Estatus </b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group text-center" id="estatus" style="color:#fff;">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Enviada</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group momento" id="enviado">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Cita</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group momento" id="fec_cita">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Asesor</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="agente">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Cliente</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="cliente">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Vehículo</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="vehiculo">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Kilometros </b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="kilometros">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2" style="display:none;">
                              <div class="form-group">
                                  <label for="Modelo"><b>Falla </b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4" style="display:none;">
                              <div class="form-group" id="falla">
                              </div>
                          </div>
                          <div class="col-md-12 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Tipo y motivo de la cita</b></label>
                              </div>
                          </div>
                          <div class="col-md-12 mb-2">
                              <div class="form-group" id="motivo">
                              </div>
                          </div>
                          <div class="col-md-12 mb-2" style="" id="conte_observacion">
                              <label for="Modelo"><b>Observación para ser enviada al cliente.</b></label>
                              <div class="form-group">
                                <textarea class="form-control" id="observacion"></textarea>
                              </div>
                          </div>
                          <div class="col-md-12 mb-2">
                              <div style="display:none;" class="text-center alert alert-info" id="mensajes"></div>
                              <div style="display:none;" class="img_cargando" id="img_cargando"><img src="../static/img/cargando.gif" style="width:50px;"></div>
                          </div>
                          <div class="modal-footer" style="" id="botones">
                            <input type="button" style="display:none;"class="btn btop btn-info" value="Aprobar cita" id="bt_aprobar" id_cita="">
                            <input type="button" style="display:none;"class="btn btop btn-danger" value="Rechazar" id="bt_rechazar" id_cita="">
                            <input type="button" style="display:none;"class="btn btop btn-success" value="Marcar como realizada" id="bt_finalizar" id_cita="">
                            <input type="button" style="display:none;"class="btn btop btn-danger" value="Cancelar la cita" id="bt_cancelar" id_cita="">
                          </div>
                      </div>
                  </form>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
    $(document).ready(function(){
      moment.locale('es');         // en
      $(".momento1").each(function(){
        $(this).text(moment($(this).text()).format('lll'));
      });
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nueva cita");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Guardar');
        $("input[name='pla']").val('');
        $("input[name='ser1']").val('');
        $("input[name='ser2']").val('');
        $("input[name='ser3']").val('');
        $("input[name='ser4']").val('');
        $("input[name='ano']").val('');
        $("select[name='mod']").val('');
      });

      $(".btop").on('click', function(){
        var idc = $(this).attr('id_cita');
        var idt = $(this).attr('id');
        var obs = $("#observacion").val().trim();
        
        if(confirm('Esta seguro?')){
          $(".img_cargando").show();
          $("#mensajes").removeClass("alert-success");
          $("#mensajes").removeClass("alert-danger");
          $("#botones").hide();
          var obj = { 
            modulo: 'citas',
            tipo: 'cambiarEstatus',
            idc: idc,
            valor: null,
            ida: '<?php echo $ida;?>',
            obs: obs
          }

          if(idt == 'bt_aprobar'){
            obj.valor = 1;
            var bt = $("#bt_aprobar");
          }else if(idt == 'bt_finalizar'){
            obj.valor = 2;
            var bt = $("#bt_finalizar");
          }else if(idt == 'bt_rechazar'){
            obj.valor = -2;
            var bt = $("#bt_rechazar");
          }else if(idt == 'bt_cancelar'){
            obj.valor = -1;
            var bt = $("#bt_cancelar");
          }

          $.post('ajax_php.php', obj, function(data){
            console.log(data);
            $(".img_cargando").hide();
            $("#mensajes").show();
            $("#botones").show();
            if(data.r == true){
              $("#mensajes").addClass("alert-success").text(data.msj);
              actualizarEvento(data);
              bt.hide();
            }else
              $("#mensajes").addClass("alert-danger").text(data.msj);
          });
        }
      });

      function actualizarEvento(obj){

        $("#estado_" + obj.cita.id).css('background', obj.cita.color);
        $("#estado_" + obj.cita.id).text(obj.cita.txt);

      }

      $(document).on('click', '.bt_detalle', function(){
        var idd = this.id;
        $("#md-cita").modal("show");
        $(".img_cargando").show();
        $.post('ajax_php.php', {modulo: 'citas', tipo: 'obtenerCita', idc: idd}, function(data){
          $(".img_cargando").hide();
          if(data.r == false) {
            alert(data.msj);
            return;
          }
          var dataCita = data.cita;
              $("#estatus").text(dataCita.txt).css('background', dataCita.color);
              $("#fec_cita").text(dataCita.fecha)
              $("#agente").text("("+dataCita.cod_age+") " + dataCita.nom_age + " " + dataCita.ape_age);
              $("#cliente").text("("+dataCita.ced_usu+") " + dataCita.nom_usu + " " + dataCita.ape_usu + " - " + dataCita.tel_usu);
              $("#vehiculo").text("("+dataCita.placa+") " + dataCita.serial1 + " AÑO: " + dataCita.ano + " - " + dataCita.modelo);
              $("#enviado").text(dataCita.fec_env);
              $("#kilometros").text(dataCita.kilometros);
              //$("#falla").text(dataCita.falla);
              $("#observacion").text(dataCita.observacion);
              $("#motivo").text(dataCita.tipo_cita+": " + dataCita.motivo);
              $("#md-cita").modal('show');
              $("#mensajes").hide();
              $(".btop").each(function(){
                $(this).attr('id_cita', dataCita.idc);
              });
              $(".momento").each(function(){
                $(this).text(moment($(this).text()).format('llll'));
              });
              $(".btop").show();
              if(dataCita.estatus == 0){
              }else if(dataCita.estatus == 1)
                $("#bt_aprobar").hide();
              else if(dataCita.estatus == -1)
                $("#bt_cancelar").hide();
              else if(dataCita.estatus == -2)
                $("#bt_rechazar").hide();
              else if(dataCita.estatus == 2){
                $("#bt_finalizar").hide();
                $("#bt_aprobar").hide();
              }
        });
      });
    });

</script>

