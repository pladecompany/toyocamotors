<?php
  include_once("controlador/admins.php");
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Usuarios del sistema</h4>
		
		<div class="text-right">
			<a href="#md-noticia" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Registrar asesor</b></a>
		</div>
	</div>

	<div class="card-body">
        <?php include_once("vistas/mensajes.php");?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre y apellido</th>
						<th>Correo de acceso</th>
						<th>Nivel de acceso</th>
						<th>Acciones</th>
					</tr>
				</thead>

				<tbody>
                  <?php
                    $noti = new Admin();
                    $r = $noti->fetchAll();
                    $i=0;
                    while($ff = $r->fetch_assoc()){
                      $i++;
                      echo "<tr>";
                      echo "  <td>" . $i . "</td>";
                      echo "  <td>" . $ff['usuario'] . "</td>";
                      echo "  <td>" . strtolower($ff['correo']) . "</td>";
                      echo "  <td>" . $noti->getNivel($ff['acceso']."") . "</td>";
                      echo "  <td>";
                      echo "<a href='?op=admins&id=".$ff['id']."'><i class='mr-2 fa fa-edit'></i></a>";
                      echo "<a href='?op=admins&el=".$ff['id']."' onclick='return confirm(\"¿ Esta seguro ?\")'><i class='mr-2 fa fa-trash'></i></a>";
                      echo "</td>";
                      echo "</tr>";
                    }
                  ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-noticia" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
                    <?php if(isset($F)){ ?>
					<h3 class="title-d" id="titulo_modulo">Editar usuario</h3>
                    <?php }else{?>
					<h3 class="title-d" id="titulo_modulo">Nuevo usuario</h3>
                    <?php }?>
				</div>

				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_noticia">
                    <?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
					<div class="row">
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Nivel de acceso</label>
                                  <?php
                                    $niveles = $noti->getNiveles();
                                    ?>
                                <select name="niv" class="form-control" required>
                                  <option value="">Seleccione</option>
                                  <?php
                                  foreach ($niveles as $item => $value){
                                    echo "<option value='$item' ".((isset($F) && $F['acceso']==$item)?'selected':'').">$value</option>";
                                  }
                                  ?>
                                </select>
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Correo de acceso</label>
								<input type="email" class="form-control form-control-lg form-control-a" placeholder="" name="cor" value="<?php echo $F['correo'];?>" required>
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Nombre y apellido</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="" name="usu" value="<?php echo $F['usuario'];?>">
							</div>
						</div>
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Título">Contraseña de acceso <?php if(isset($F)) echo '(Para cambiar la contraseña, introduzca una nueva de lo contrario dejar la caja vacia)';?></label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="" name="pas" value="" <?php if(!isset($F)) echo 'required';?>>
							</div>
						</div>
					</div>
                    <div class="modal-footer">
                        <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Guardar')?></button>
                    </div>
				</form>
			</div>


		</div>
	</div>
</div>

<?php
  if(isset($F)){
?>
  <script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").trigger('click');
      $("select[name='niv']").val(<?php echo $F['acceso'];?>);
    });
  </script>

<?php
  } 
?>
<script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nuevo usuario");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Guardar');
        $("input[name='tit']").val('');
        $("input").val('');
        $("textarea[name='des']").val('');
        $("select[name='est']").val('');
        $("#cont_img").remove();
      });
    });

</script>

