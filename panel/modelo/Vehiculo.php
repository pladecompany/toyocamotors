<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Vehiculo{
    private $tabla = "vehiculos";
    public $data = [];
    public $orm = null;

    public function Vehiculo($tabla){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, modelos M WHERE V.id_modelo=M.id ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchVehiculosByModelo($idm){
      $sql = "SELECT * FROM ".$this->tabla." WHERE id_modelo='$idm' AND est_veh='1' ORDER BY nom_veh;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchVehiculosByImportado($idm){
      $sql = "SELECT * FROM ".$this->tabla." WHERE imp_veh='$idm' AND est_veh='1' ORDER BY nom_veh;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchVehiculosActivos(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_veh='1' ORDER BY nom_veh;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchVehiculosActivosExiauto(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE sto_veh='1' AND est_veh='1' ORDER BY nom_veh;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
