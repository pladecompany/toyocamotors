<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Factura{

    private $tabla = "facturas";
    public $data = [];
    public $data2 = [];
    public $orm = null;

    public function Factura(){
      $tihs->data = [];
      $this->orm = new Orm(new Conexion());
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT *, F.id as idf FROM ".$this->tabla." F, clientes C, admins A WHERE F.id_admin=A.id AND F.id_cliente=C.id AND F.id=$id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchDetalles($idf){
      $sql = "SELECT *, D.can_pro as cantidad, D.pre_pro as precio FROM detalles_factura D, inventario I, categorias C WHERE D.id_factura=$idf AND D.id_producto=I.id AND I.id_categoria=C.id;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAll(){
      $sql = "SELECT *, F.id as idf, (SELECT sum(D.can_pro*D.pre_pro) FROM detalles_factura D WHERE D.id_factura=F.id) as monto  FROM ".$this->tabla." F, clientes C, admins A WHERE F.id_admin=A.id AND F.id_cliente=C.id ORDER BY F.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

    public function saveDetails($id, $prods, $cants){
      $n = 0;
      for($i = 0; $i < count($prods); $i++){
        $inv = new Inventario();
        $rinv = $inv->findById($prods[$i]);
        $sql = "INSERT INTO detalles_factura VALUES(null, $id, '".$rinv['idc']."', '".$rinv['nom_pro']."', '".$rinv['des_pro']."', '".$rinv['pre_pro']."', '".$cants[$i]."' );";
        $r = $this->orm->insertarPersonalizado($sql);
        if($r != false){
          $n++;
          $sql = "UPDATE inventario SET can_pro=can_pro-".$cants[$i]." WHERE id=".$rinv['idc'].";";
          $this->orm->editarPersonalizado($sql);
        }
      }
      return $n;
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
