<?php

  if(isset($_POST) && isset($_POST['btg'])){
    include_once("../modelo/Cliente.php"); 

    $ced1 = $_POST['cod1'];
    $ced2 = $_POST['cod2'];
    $nom = $_POST['nom'];
    $ape = $_POST['ape'];
    $cor = $_POST['cor'];
    $tlf1 = $_POST['tlf1'];
    $tlf = $tlf1.$_POST['tlf'];
    $emp = $_POST['emp'];
    $pas = $_POST['pas'];
    $cpa = $_POST['cpa'];
    $fecn = '';
    if(empty($ced1)||$ced1=="")
      $err = "Cédula invalida";
    else if(empty($ced2) || $ced2 == "")
      $err = "Cédula invalida";
    else if(empty($nom)||$nom == "")
      $err = "Nombre está vacío";
    else if(empty($tlf)||$tlf == "")
      $err = "Teléfono está vacío";
    else if(empty($ape)||$ape == "")
      $err = "Apellido está vacío";
    else if(empty($cor)||$cor == "")
      $err = "Correo está vacío";
    else if(empty($pas)||$pas == "")
      $err = "Contraseña está vacía";
    else if($pas != $cpa)
      $err = "Contraseña no coinciden";

    if(isset($err)){
      echo "<script>window.location ='../../?reg&err&msj=$err';</script>";
      exit(1);
    }


    $cliente = new Cliente();
    $cliente->data["id"] = "";
    $cliente->data["ced_usu"] = $ced1."-".$ced2;
    $cliente->data["nom_usu"] = $nom;
    $cliente->data["ape_usu"] = $ape;
    $cliente->data["cor_usu"] = $cor;
    $cliente->data["tel_usu"] = $tlf;
    $cliente->data["pas_usu"] = $pas;
    $cliente->data["fec_nac_usu"] = $fecn;
    $cliente->data["fec_reg_usu"] = date("Y-m-d H:i:s");
    $cliente->data["emp_usu"] = $emp;

    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      session_start();
      $_SESSION['log'] = true;
      $_SESSION['idu']=$id;
      $_SESSION['nom']=$nom;
      echo "<script>window.location ='../../index.php?op=inicio_log&log&info&msj=Bienvenido';</script>";
      exit(1);
    }else{
      $err = "¡Cédula/Correo ya existe!";
      echo "<script>window.location ='../../?reg&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){
    include_once("../modelo/Cliente.php"); 

    $ced1 = $_POST['cod1'];
    $ced2 = $_POST['cod2'];
    $nom = $_POST['nom'];
    $ape = $_POST['ape'];
    $cor = $_POST['cor'];
    $tlf1 = $_POST['tlf1'];
    $tlf = $tlf1.$_POST['tlf'];
    $emp = $_POST['emp'];
    $fecn = $_POST['fec'];

    if(empty($ced1)||$ced1=="")
      $err = "Cédula invalida";
    else if(empty($ced2) || $ced2 == "")
      $err = "Cédula invalida";
    else if(empty($nom)||$nom == "")
      $err = "Nombre está vacío";
    else if(empty($ape)||$ape == "")
      $err = "Apellido está vacío";
    else if(empty($cor)||$cor == "")
      $err = "Correo está vacío";

    if(isset($err)){
      echo "<script>window.location ='?op=perfil&reg&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Cliente();
    $cliente->data["ced_usu"] = $ced1."-".$ced2;
    $cliente->data["nom_usu"] = $nom;
    $cliente->data["ape_usu"] = $ape;
    $cliente->data["cor_usu"] = $cor;
    $cliente->data["tel_usu"] = $tlf;
    $cliente->data["fec_nac_usu"] = $fecn;
    $cliente->data["fec_reg_usu"] = date("Y-m-d H:i:s");
    $cliente->data["emp_usu"] = $emp;
    session_start();
    $r = $cliente->edit($_SESSION['idu']);
    if($r){
      echo "<script>window.location ='../../?op=perfil&info&msj=Información actualizada';</script>";
      exit(1);
    }else{
      echo "<script>window.location ='../../?op=perfil&info&msj=Información actualizada';</script>";
      exit(1);
    }


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Cliente();
    $r = $cliente->findById($id);

    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=clientes&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Cliente();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=clientes&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=clientes&err&msj=$err';</script>";
      exit(1);
    }
    exit(1);
  }else if(isset($_POST['bt_clave'])){
    include_once("../modelo/Cliente.php"); 
    $act = $_POST['act'];
    $pas = $_POST['pas'];
    $cpa = $_POST['cpa'];

    if($pas != $cpa){
      echo "<script>window.location ='../../?op=perfil&err&msj=Contraseña no coinciden.';</script>";
      exit(1);
    }

    $cliente = new Cliente();
    session_start();
    $r = $cliente->cambiarClave($act, $pas, $_SESSION['idu']);

    if($r){
      echo "<script>window.location ='../../?op=perfil&cc&info&msj=Contraseña actualizada correctamente.';</script>";
      exit(1);
    }else{
      echo "<script>window.location ='../../?op=perfil&cc&err&msj=La contraseña actual es invalida.';</script>";
      exit(1);
    }


  }else{
    include_once("modelo/Cliente.php"); 
  }
?>
