<?php
  include_once("panel/modelo/Citas.php");
  include_once("panel/modelo/Agente.php");
  include_once("panel/modelo/MiVehiculo.php");

  if(isset($_POST['bt_agendar'])){

    $age = new Agente();
    $fage = $age->findById($_POST['ida']);
    if($fage == false){
      echo "<script>window.location ='?op=cita';</script>";
      exit(1);
    }

    $veh = new MiVehiculo();
    $fveh = $veh->verificarVehiculo($_SESSION['idu'], $_POST['idv']);

    
    if($fveh == false){
      echo "<script>window.location ='?op=cita';</script>";
      exit(1);
    }
    $ida = $_POST['ida'];
    $idv = $_POST['idv'];
    $idu = $_POST['idu'];
    $mot = $_POST['mot'];
    $tip = $_POST['tip'];
    $kil = $_POST['kil'];
    $rev = $_POST['rev'];
    $idu = $_SESSION['idu'];
    $fec = $_POST['fec'];

    if($age->validarHorario($ida, $fec)){
      $msj = "El asesor seleccionado no tiene disponibilidad para la fecha: $fec";
      echo "<script>window.location ='?op=cita&err&msj=$msj';</script>";
      exit(1);
    }

    $cita = new Cita();
    $cita->data["id"] = '';
    $cita->data["id_usuario"] = $idu;
    $cita->data["id_vehiculo"] = $idv;
    $cita->data["id_agente"] = $ida;
    $cita->data["motivo"] = $mot;
    $cita->data["fecha"] = $fec;
    $cita->data["estatus"] = '0';
    $cita->data["fec_env"] = date('Y-m-d H:i:s');
    $cita->data["fec_res"] = null;
    //$cita->data["tipo_cita"] = $tip;
    $cita->data["kilometros"] = $kil;
    $cita->data["id_falla"] = $rev;
    $cita->data["observacion"] = "";
    $cita->data["id_tipo_cita"] = $tip;

    $r = $cita->save();
    if($r->affected_rows == 1){
      $msj = "Su solicitud de cita fue enviada correctamente";
      echo "<script>window.location ='?op=cita&info&msj=$msj';</script>";
    }else{
      $msj = "No se pudo agendar su cita, intentelo nuevamente";
      echo "<script>window.location ='?op=cita&err&msj=$msj';</script>";
    }

    exit(1);

  }else if(isset($_GET['calendario']) && isset($_GET['veh']) && isset($_GET['age'])){

    // Se comento la validacion, para que deje dos citas del mismo vehiculo
    //$cita = new Cita();
    //$rc = $cita->verificarCitaAgendada($_SESSION['idu'], $_GET['veh']);

    //if($rc == false){
    //}else{
    //  $msj = "Usted ya tiene una cita agendada, para el vehiculo seleccionado. ";
    //  echo "<script>window.location ='?op=cita&err&msj=$msj';</script>";
    //  exit(1);
    //}
    
    $age = new Agente();

    $fage = $age->findById($_GET['age']);
    if($fage == false){
      echo "<script>window.location ='?op=cita&kk';</script>";
      exit(1);
    }
    $r_citas = $age->citasPendientes($_GET['age']);

    $veh = new MiVehiculo();

    $fveh = $veh->verificarVehiculo($_SESSION['idu'], $_GET['veh']);
    
    if($fveh == false){
      echo "<script>window.location ='?op=cita&err=No existe vehículo';</script>";
      exit(1);
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cita = new Cita();
    $r = $cita->cancelarCita($id, $_SESSION['idu']);
    if($r){
      echo "<script>window.location ='?op=cita&info&msj=Cita cancelada correctamente';</script>";
      exit(1);
    }else{
      echo "<script>window.location ='?op=cita&err&msj=No se pudo cancelar la cita';</script>";
      exit(1);
    }
  }

?>
