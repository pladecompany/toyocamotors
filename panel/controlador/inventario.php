<?php
  include_once("modelo/Inventario.php"); 
  include_once("modelo/Categoria.php"); 
  include_once("modelo/Orm.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $cod = $_POST['cod_pro'];
    $nom = $_POST['nom_pro'];
    $des = $_POST['des_pro'];
    $cat = $_POST['cate'];
    $can = $_POST['can_pro'];
    $pre = $_POST['pre_pro'];

    if(strlen($cod) == 0){
      $err = "Debe llenar el campo código.";
    }else if(strlen($nom) < 2){
      $err = "El campo nombre debe tener al menos 2 carácteres";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=inventario&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Inventario();

    $cliente->data["id"] = "";
    $cliente->data["id_categoria"] = $cat;
    $cliente->data["cod_pro"] = $cod;
    $cliente->data["nom_pro"] = $nom;
    $cliente->data["des_pro"] = $des;
    $cliente->data["pre_pro"] = $pre;
    $cliente->data["can_pro"] = $can;
    $cliente->data["fec_reg_pro"] = date("Y-m-d H:i:s");
    $cliente->data["img_pro"] = "";

    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      
      if(!empty($_FILES['img'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/static/imgs_productos/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/panel/static/imgs_productos/".$nom1;
        if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
          $producto = new Inventario();
          $producto->data['img_pro'] = $nf;
          $producto->edit($id);
        }
      }
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=inventario&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=inventario&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $cod = $_POST['cod_pro'];
    $nom = $_POST['nom_pro'];
    $des = $_POST['des_pro'];
    $cat = $_POST['cate'];
    $can = $_POST['can_pro'];
    $pre = $_POST['pre_pro'];

    if(strlen($cod) == 0){
      $err = "Debe llenar el campo código.";
    }else if(strlen($nom) < 2){
      $err = "El campo nombre debe tener al menos 2 carácteres";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=inventario&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Inventario();

    $cliente->data["id_categoria"] = $cat;
    $cliente->data["cod_pro"] = $cod;
    $cliente->data["nom_pro"] = $nom;
    $cliente->data["des_pro"] = $des;
    $cliente->data["pre_pro"] = $pre;
    $cliente->data["can_pro"] = $can;

    if(!empty($_FILES['img'])){
      $orm = new Orm(new Conexion());
      $ruta = getcwd() . "/static/imgs_productos/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/panel/static/imgs_productos/".$nom1;
      if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
        $cliente->data["img_pro"] = $nf;
      }
    }

    $id = $_POST['idc'];
    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=inventario&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=inventario&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Inventario();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=inventario&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Inventario();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=inventario&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=inventario&err&msj=$err';</script>";
    }
    exit(1);
  }else if(isset($_POST) && isset($_POST['bt_salida'])){
    $can = $_POST['cans'];
    $pro = $_POST['prods'];
    $mot = $_POST['motivo'];

    $inv = new Inventario();
    $r = $inv->saveEntrada($pro, $can, $mot, '-');

    if($r == true){
      echo "<script>window.location ='?op=salidas&info&msj=La salida se procesó correctamente';</script>";
    }else{
      echo "<script>window.location ='?op=salidas&err&msj=No se pudo a procesar la salida';</script>";
    }
    exit(1);
  }else if(isset($_POST) && isset($_POST['bt_entrada'])){
    $can = $_POST['cans'];
    $pro = $_POST['prods'];
    $mot = $_POST['motivo'];

    $inv = new Inventario();
    $r = $inv->saveEntrada($pro, $can, $mot, '+');

    if($r == true){
      echo "<script>window.location ='?op=entradas&info&msj=La entrada se procesó correctamente';</script>";
    }else{
      echo "<script>window.location ='?op=entradas&err&msj=No se pudo a procesar la entrada';</script>";
    }
    exit(1);
  }else if(isset($_GET['entradas_salidas'])){
    $idp = $_GET['entradas_salidas'];
    $inv = new Inventario();
    $FP = $inv->findById($idp);
    if($FP==false){
        echo "<script>window.location ='?op=inventario';</script>";
        exit(1);
    }else{
      $all = $inv->fetchEntradas($idp);
    }
  }

?>
