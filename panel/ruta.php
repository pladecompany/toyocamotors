<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	
	$opcion = $_GET['op'];
	switch ($opcion) {
		case "inicio":
			$ruta="vistas/inicio.php";
		break;


        // Admin, asesor de citas, clientes
		case "citas":
			$ruta="vistas/citas.php";
		break;
		case "solicitudes":
			$ruta="vistas/solicitudes.php";
		break;
		case "clientes":
			$ruta="vistas/clientes.php";
		break;

		case "configuraciones":
			$ruta="vistas/configuraciones.php";
		break;

		case "noticias":
			$ruta="vistas/noticias.php";
		break;

		case "asesores":
			$ruta="vistas/asesores.php";
		break;
		case "admins":
			$ruta="vistas/admins.php";
		break;

        // Admin y vehiculos
		case "vehiculos":
			$ruta="vistas/vehiculos.php";
		break;

		case "modelos":
			$ruta="vistas/modelos.php";
		break;

		case "vehiculo":
			$ruta="vistas/vehiculo.php";
		break;

		case "slider":
			$ruta="vistas/slider.php";
		break;

		case "cambiarcontraseña":
			$ruta="vistas/cambiarcontraseña.php";
		break;

		default:
			header('Location: ?op=inicio');
		break;
	}
?>
