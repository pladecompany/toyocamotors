<div id="md-promo" class="modal fade" tabindex="-1" role="dialog" style="">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header text-center">
              <h3 class="text-center"><?php echo $promo['titulo'];?></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body" style="text-align:center;">
            <?php
              if($promo['tipo'] == 'video'){ 
            ?>
				<video class="responsive-video" controls style="width: 100%;margin:auto;" id="video_modal">
					<source src="<?php echo $promo['url'];?>" type="video/mp4">
				</video>
            <?php }else{ ?>
              <img src="<?php echo $promo['url'];?>" style="width:100%;">
            <?php } ?>

          </div>
      </div>
  </div>
</div>

