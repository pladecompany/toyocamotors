<?php
  include_once("panel/modelo/Noticia.php");
  $noti_leer = new Noticia();
  
  $idn = $_GET['id'];
  $NOTI = $noti_leer->findById($idn);
  if($NOTI == false){
    echo "<script>window.location ='index.php';</script>";
    exit(1);
  }
  $feca = explode("-", $NOTI['fec_reg_noti']);
  
?>

<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/1.jpg');" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate text-center">
				<h1 class="mb-2 bread">Noticias</h1>
				<p class="breadcrumbs">
                  <span class="mr-2">
                    <a href="?op=inicio">Toyoca Motors <i class="ion-ios-arrow-forward"></i></a>
                  </span>
                  <span><a href="?op=noticias">Noticias <i class="ion-ios-arrow-forward"></i></span>
                  <span><?php echo $NOTI['titulo'];?></span>
                </p>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row m-0">
		<div class="col-sm-12 col-md-12">
			<section class="ftco-section">
				<div class="row">
					<div class="col-md-12 ftco-animate">
						<div class="blog-entry">
                            <img src="<?php echo $NOTI['img'];?>" alt="<?php echo $NOTI['titulo'];?>" class="img-fluid" style="width: 100%; height: auto;">
							<div class=" p-3">
								<div class="meta">
                                    <div><a href="#" class="momento">Publicado el <?php echo $feca[2]." del mes". $feca[1]." del ".$feca[0];?></a></div>
								</div>
							</div>
                            <h3 class="heading"><a href="#"><?php echo $NOTI['titulo'];?></a></h3>
							<p class="text-justify">
                                <?php echo nl2br($NOTI['descripcion']);?>
                            </p>
							<a href="?op=noticias" class="text-center"><i class="fa fa-angle-double-left"></i> Volver a noticias</a>
						</div>
					</div>
				</div>
			</section>
		</div>

	</div>
</div>
