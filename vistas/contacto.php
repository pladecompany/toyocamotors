<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/contacto.jpg');" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate text-center">
				<h1 class="mb-2 bread">Contáctanos</h1>
				<p class="breadcrumbs">
					<span class="mr-2"><a href="?op=inicio">Toyoca Motors <i class="ion-ios-arrow-forward"></i></a></span>
					<span>Contacto <i class="ion-ios-arrow-forward"></i></span></p>
			</div>
		</div>
	</div>
</section>

<section class="ftco-section ftco-no-pt ftco-no-pb contact-section">
	<div class="container">
		<div class="row d-flex align-items-stretch no-gutters">
			<div class="col-md-6 p-5">
				<h2 class="h4 mb-5 font-weight-bold">Contáctanos</h2>
				<form action="#">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Nombre">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Correo electrónico">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Título">
					</div>
					<div class="form-group">
						<textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Mensaje"></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary py-3 px-5"><i class="fa fa-paper-plane"></i> Enviar mensaje</button>
					</div>
				</form>
			</div>

			<div class="col-md-6 p-5">
				<div class="container">
					<div class="row d-flex contact-info">
						<div class="col-md-12 mb-4">
							<h2 class="h4 font-weight-bold">Información de contacto</h2>
						</div>
						<div class="w-100"></div>
						<div class="col-md-12 d-flex">
							<div class="dbox">
								<p><i class="fa fa-map-pin"></i> <b>Dirección:</b><br> <a href="#">Av. Principal de los Ruices. Edificio Stemo. PB, locales 1,2,3. Caracas - Venezuela</a></p>
							</div>
						</div>
						<div class="col-md-12 d-flex">
							<div class="dbox">
								<p><i class="fa fa-phone"></i> <b>Teléfono:</b><br> <a href="tel://1234567920">0212-2392024</a></p>
							</div>
						</div>
						<div class="col-md-12 d-flex">
							<div class="dbox">
								<p><i class="fa fa-envelope"></i> <b>Correo:</b><br> <a href="mailto:info@yoursite.com">info@toyocamotors.com</a></p>
							</div>
						</div>
						<div class="col-md-12 d-flex">
							<div class="dbox">
								<p><i class="fa fa-laptop"></i> <b>Página web</b><br> <a href="#">www.toyocamotors.com</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="contact-section bg-light">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1961.5757847906068!2d-66.828731189731!3d10.488715423611518!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c2a5835b0643ef1%3A0x6a86309632922d2!2sToyoca%20Motors%20C.A!5e0!3m2!1ses!2sve!4v1570299970610!5m2!1ses!2sve" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>		
</section>
