<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/servicio.jpg');" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate text-center">
				<h1 class="mb-2 bread">Servicio</h1>
				<p class="breadcrumbs"><span class="mr-2"><a href="?op=inicio">Toyoca Motors <i class="ion-ios-arrow-forward"></i></a></span> <span>Servicio <i class="ion-ios-arrow-forward"></i></span></p>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="col-lg-8 offset-lg-2 pt-4 pb-4 ftco-animate fadeInUp ftco-animated">

		<p class="text-justify">Mantener tu <b class="clr_red">TOYOTA</b> en óptimas condiciones es importante. En Toyoca Motors, brindamos <b>calidad y seguridad</b> a nuestros clientes, atención personalizada y un excelente servicio  desde nuestras cómodas instalaciones y confortables sala de espera.</p>

		<p class="text-justify">Nuestro equipo está conformado por <b>asesores y técnicos especializados</b>, altamente capacitados y certificados; la experiencia y el conocimiento de nuestros técnicos les permiten darle <b>el mejor cuidado y atender las necesidades de su automóvil.</p>

		<p class="text-justify">Además para su mayor comodidad, tenemos el servicio de solicitud de citas en nuestra web, donde podrás agendar cualquier día hábil de la semana e informarte todo acerca del status de tu automóvil.</p><br>

		<div class="row">
			<div class="col-xs-12 col-lg-8">
				<h4 class="text-center clr_red">Importancia del servicio de mantenimiento</h4>
				<p><i class="fa fa-check-circle clr_red"></i> Realizarlo cada 3 meses o 5.000 km lo que ocurra primero te permite conservar tu Toyota en optimas condiciones.</p>

				<p><i class="fa fa-check-circle clr_red"></i> Conservas la validez de la garantía por 2 años o 50.000 km lo que ocurra primero.</p>

				<p><i class="fa fa-check-circle clr_red"></i> Al tenerlo en óptimas condiciones aseguras un mayor valor de recompra a futuro.</p>
			</div>

			<div class="col-xs-12 col-lg-4 text-center">
				<img src="static/img/car.png" class="img-textos">
			</div>
		</div>

		<div class="text-center mb-5">
            <a href="<?php if(isset($_SESSION['log']) && $_SESSION['log'] == true) echo '?op=cita';else echo '#';?>" <?php if(!isset($_SESSION['log'])){ ?> data-toggle="modal" data-target="#modalIngresar" <?php }?> class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Solicitar cita</a>
		</div>
	</div>
</div>
