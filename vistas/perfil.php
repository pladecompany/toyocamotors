<?php
if(!isset($_SESSION['log'])){
	session_start();
	session_destroy();
	echo "<script>window.location ='index.php';</script>";
	exit(1);
}else{
	include_once("panel/controlador/misvehiculos.php");
	include_once("panel/modelo/Cliente.php");
	include_once("panel/controlador/clientes.php");

	$cli = new Cliente();
	$F = $cli->findById($_SESSION['idu']);

	if($F == false){
	echo "<script>window.location ='salir.php';</script>";
	exit(1);
	}
	$ced = $F['ced_usu'];
	$a = explode("-", $ced);
	$ced1 = $a[0];
	$ced2 = $a[1];
}
?>

<div class="header"></div>



<section class="ftco-section ftco-no-pt ftco-no-pb contact-section">
	<div class="container">
		<div class="row d-flex align-items-stretch no-gutters">
			<div class="col-md-6 p-2">
				<div class="form">
					<h2 class="h4 m-0 font-weight-bold text-center">Información personal</h2><hr>
					<?php 
						if(!isset($_GET['cc'])) {
						include_once("mensajes.php");
						}
					?>

					<form class="form-a" action="panel/controlador/clientes.php" id="formulario_registro_cliente" method="POST">
						<div clas="col-md-12 text-center" style="display:none;text-align:center !important;" id="cont_msj">
							<span id="txt_msj" style="color:red;text-align:center;"></span>
							<br><br>
						</div>

						<div class="row">
							<div class="col-md-4">
								<label for="Correo">V/E/J</label>
								<select class="form-control form-control-lg form-control-a" name="cod1" required>
									<option <?php if($ced1 == 'V') echo 'selected';?>>V</option>
									<option <?php if($ced1 == 'E') echo 'selected';?>>E</option>
									<option <?php if($ced1 == 'J') echo 'selected';?>>J</option>
								</select>
							</div>

							<div class="col-md-8">
								<label for="Correo">Cédula/rif</label>
								<div class="form-group">
									<input type="text" class="form-control form-control-lg form-control-a" placeholder="Cédula/Rif" name="cod2" required minlength="3" value="<?php echo $ced2;?>">
								</div>
							</div>

							<?php if($ced1 == 'J') { ?>
							<div class="col-md-8">
								<div class="form-group">
									<label for="Correo">Razón social o empresa</label>
									<input type="text" class="form-control form-control-lg form-control-a" placeholder="Razón social o empresa" name="emp"value="<?php echo $F['emp_usu'];?>">
								</div>
							</div>
							<?php } ?>

							<div class="col-md-12">
								<div class="form-group">
									<label for="Nombres">Nombres</label>
									<input type="text" class="form-control form-control-lg form-control-a" placeholder="Nombres" required minlength="3" name="nom" value="<?php echo $F['nom_usu'];?>">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="Apellidos">Apellidos</label>
									<input type="text" class="form-control form-control-lg form-control-a" placeholder="Apellidos" required minlength="3" name="ape" value="<?php echo $F['ape_usu'];?>">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="Fecha de nacimiento">Fecha de nacimiento</label>
									<input type="date" class="form-control form-control-lg form-control-a" required name="fec" value="<?php echo $F['fec_nac_usu'];?>">
								</div>
							</div>

							<div class="col-md-12">
								<label for="Teléfono">Teléfono de contacto</label>
								<div class="row">
									<div class="form-group col-md-5">
										<select name="tlf1" class="form-control" required>
										<option value="">--</option>
										<option <?php if(substr($F['tel_usu'], 0, 4) =='0412') echo 'selected';?>>0412</option>
										<option <?php if(substr($F['tel_usu'], 0, 4) =='0424') echo 'selected';?>>0424</option>
										<option <?php if(substr($F['tel_usu'], 0, 4) =='0414') echo 'selected';?>>0414</option>
										<option <?php if(substr($F['tel_usu'], 0, 4) =='0416') echo 'selected';?>>0416</option>
										<option <?php if(substr($F['tel_usu'], 0, 4) =='0426') echo 'selected';?>>0426</option>
										</select>
									</div>
									<div class="form-group col-md-7">
										<input type="text" class="form-control form-control-lg form-control-a" placeholder="Teléfono" name="tlf" required maxlength="7" minlength="7" value="<?php echo substr($F['tel_usu'], 4, strlen($F['tel_usu']));?>">
									</div>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
									<label for="Correo">Correo</label>
									<input type="email" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo" required minlength="5" name="cor" value="<?php echo $F['cor_usu'];?>">
								</div>
							</div>

							<div class="col-md-12 text-center">
								<div class="form-group">
									<label for="Contraseña">&nbsp;</label>
								</div>
							</div>

							<div class="col-md-12 text-center">
								<button type="submit" name="btc" class="btn btn-primary" style="width:50%;">Guardar </button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-6 p-2">
				<div class="form">
					<h2 class="h4 m-0 font-weight-bold text-center">Cambiar contraseña</h2><hr>
					<?php 
						if(isset($_GET['cc'])) {
						include_once("mensajes.php");
						}
					?>

					<form class="form-a" action="panel/controlador/clientes.php" id="formulario_registro_cliente" method="POST">
						<div clas="col-md-12 text-center" style="display:none;text-align:center !important;" id="cont_msj">
							<span id="txt_msj" style="color:red;text-align:center;"></span>
							<br><br>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="Contraseña">Contraseña actual</label>
									<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" required minlength="5" name="act">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="Contraseña">Nueva contraseña</label>
									<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" required minlength="5" name="pas">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="Contraseña">Confirmar contraseña</label>
									<input type="password" class="form-control form-control-lg form-control-a" placeholder="Confirma tu contraseña" required minlength="5" name="cpa">
								</div>
							</div>

							<div class="col-md-12 text-center">
								<div class="form-group">
									<label for="Contraseña">&nbsp;</label>
									<button type="submit" name="bt_clave" class="btn btn-primary" style="width:50%;">Cambiar Contraseña</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
