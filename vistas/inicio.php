<?php
   include_once("panel/modelo/Vehiculo.php");
   include_once("panel/modelo/Modelo.php");
?>
<section class="home-slider owl-carousel js-fullheight">
	<div class="slider-item js-fullheight" style="background-image: url(static/img/1.jpg);">
		<div class="overlay"></div>
		<div class="row slider-text js-fullheight justify-content-center align-items-center" data-scrollax-parent="true">
			<div class="col-md-12 col-sm-12 text-center ftco-animate">
				<h1 class="mb-4 mt-5">Toyoca Motors, calidad y durabilidad</h1>
				<p>
					<a href="#nosotros" class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-search"></i> Ver más</a>
					<a href="?op=contacto" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Contáctanos</a>
				</p>
			</div>
		</div>
	</div>

	<div class="slider-item js-fullheight" style="background-image: url(static/img/2.jpg);">
		<div class="overlay"></div>
			<div class="row slider-text js-fullheight justify-content-center align-items-center" data-scrollax-parent="true">
			<div class="col-md-12 col-sm-12 text-center ftco-animate">
				<h1 class="mb-4 mt-5">Brindamos el servicio que tú vehículo merece</h1>
				<p>
					<a href="#nosotros" class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-search"></i> Ver más</a>
					<a href="?op=contacto" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Contáctanos</a>
				</p>
			</div>
		</div>
	</div>

	<div class="slider-item js-fullheight" style="background-image: url(static/img/3.jpg);">
		<div class="overlay"></div>
		<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
			<div class="col-md-12 col-sm-12 text-center ftco-animate">
				<h1 class="mb-4 mt-5">Equipos de avanzada tecnología</h1>
				<p>
					<a href="<?php if(isset($_SESSION['log']) && $_SESSION['log'] == true) echo '?op=cita';else echo '#';?>" <?php if(!isset($_SESSION['log'])){ ?> data-toggle="modal" data-target="#modalIngresar" <?php }?> class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Solicitar cita</a>
					<a href="?op=latorenia" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3"><i class="fa fa-search"></i> Saber más</a>
				</p>
			</div>
		</div>
	</div>

	<div class="slider-item js-fullheight" style="background-image: url(static/img/4.jpg);">
		<div class="overlay"></div>
		<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
			<div class="col-md-12 col-sm-12 text-center ftco-animate">
				<h1 class="mb-4 mt-5">Atención personalizada, excelente servicio</h1>
				<p>
					<a href="<?php if(isset($_SESSION['log']) && $_SESSION['log'] == true) echo '?op=cita';else echo '#';?>" <?php if(!isset($_SESSION['log'])){ ?> data-toggle="modal" data-target="#modalIngresar" <?php }?> class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Solicitar cita</a>
					<a href="?op=servicio" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3"><i class="fa fa-search"></i> Saber más</a>
				</p>
			</div>
		</div>
	</div>

	<div class="slider-item js-fullheight" style="background-image: url(static/img/5.jpg);">
		<div class="overlay"></div>
		<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
			<div class="col-md-12 col-sm-12 text-center ftco-animate">
				<h1 class="mb-4 mt-5">Asesores y técnicos especializados</h1>
				<p>
					<a href="<?php if(isset($_SESSION['log']) && $_SESSION['log'] == true) echo '?op=cita';else echo '#';?>" <?php if(!isset($_SESSION['log'])){ ?> data-toggle="modal" data-target="#modalIngresar" <?php }?> class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Solicitar cita</a>
					<a href="?op=contacto" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Contáctanos</a>
				</p>
			</div>
		</div>
	</div>

	<div class="slider-item js-fullheight" style="background-image: url(static/img/6.jpg);">
		<div class="overlay"></div>
		<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
			<div class="col-md-12 col-sm-12 text-center ftco-animate">
				<h1 class="mb-4 mt-5">Atendemos la necesidades de su TOYOTA </h1>
				<p>
					<a href="<?php if(isset($_SESSION['log']) && $_SESSION['log'] == true) echo '?op=cita';else echo '#';?>" <?php if(!isset($_SESSION['log'])){ ?> data-toggle="modal" data-target="#modalIngresar" <?php }?> class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Solicitar cita</a>
					<a href="?op=servicio" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3"><i class="fa fa-search"></i> Saber más</a>
				</p>
			</div>
		</div>
	</div>

	<div class="slider-item js-fullheight" style="background-image: url(static/img/7.jpg);">
		<div class="overlay"></div>
		<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
			<div class="col-md-12 col-sm-12 text-center ftco-animate">
				<h1 class="mb-4 mt-5">Agenda el control de tus servicios</h1>
				<p>
					<a href="<?php if(isset($_SESSION['log']) && $_SESSION['log'] == true) echo '?op=cita';else echo '#';?>" <?php if(!isset($_SESSION['log'])){ ?> data-toggle="modal" data-target="#modalIngresar" <?php }?> class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Solicitar cita</a>
					<a href="?op=contacto" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3"><i class="fa fa-search"></i> Saber más</a>
				</p>
			</div>
		</div>
	</div>

	<div class="slider-item js-fullheight" style="background-image: url(static/img/8.jpg);">
		<div class="overlay"></div>
		<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
			<div class="col-md-12 col-sm-12 text-center ftco-animate">
				<h1 class="mb-4 mt-5">Repuestos Originales TOYOTA</h1>
				<p>
					<a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3"><i class="fa fa-id-card"></i> Solicitar repuesto</a>
					<a href="?op=repuesto" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3"><i class="fa fa-search"></i> Saber más</a>
				</p>
			</div>
		</div>
	</div>
</section>


<section class="ftco-section bg-light pt-5" id="nosotros">
	<div class="container"  style="margin-top: 5rem;">
		<div class="row justify-content-center mb-2">
			<div class="col-md-7 text-center heading-section ftco-animate">
				<div class="heading-section mb-4 text-center">
					<span class="subheading">Nosotros</span>
					<h2 class="mb-4">Toyoca Motors</h2>
				</div>
				<p>Somos un equipo especializado para el diagnóstico de tu Vehículo, donde puedes solicitar tu cita Online y llevar el control de tus servicios. Toyota Motors te brinda el servicio Toyota, latonería, pintura para tú vehículo y repuestos originales para mantenenerlo con calidad y durabilidad.</p>
			</div>
		</div>
	</div>
</section>


<section class="ftco-section ftco-wrap-about ftco-no-pb">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-10 wrap-about ftco-animate text-center">
				<div class="heading-section mb-4 text-center">
					<span class="subheading">Beneficios</span>
					<h2 class="mb-4">Todo para tu Toyota</h2>
				</div>

				<div class="row">
					<div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
						<div class="media block-6 services d-block">
							<div class="icon d-flex justify-content-center align-items-center">
								<img src="static/img/icon-1.png" width="60%">
							</div>

							<div class="media-body p-2">
								<h3 class="heading">Servicio</h3>
								<p>Ofrecemos el mantenimiento periódico preventivo de tu vehículo, <br>solicita tu <a href="#" data-toggle="modal" data-target="#modalIngresar">Cita online</a></p>
								<a href="?op=servicio">Saber más</a>
							</div>
						</div>
					</div>

					<div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
						<div class="media block-6 services d-block">
							<div class="icon d-flex justify-content-center align-items-center">
								<img src="static/img/icon-2.png" width="70%">
							</div>

							<div class="media-body p-2 mt-3">
								<h3 class="heading">Latonería y pintura</h3>
								<p>Contamos con un equipo de técnicos especializados, <br>solicita tu <a href="#" data-toggle="modal" data-target="#modalIngresar">Cita online</a></p>
								<a href="?op=latoneria">Saber más</a>
							</div>
						</div>
					</div>

					<div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
						<div class="media block-6 services d-block">
							<div class="icon d-flex justify-content-center align-items-center">
								<img src="static/img/icon-3.png" width="75%">
							</div>

							<div class="media-body p-2 mt-3">
								<h3 class="heading">Repuestos y accesorios</h3>
								<p>Para que su Toyota se conserve siempre original tenemos para usted variedad en repuestos y accesorios originales</p>
								<a href="?op=repuestos">Saber más</a>
							</div>
						</div>
					</div>
				</div>

				<div class="video justify-content-center">
					<a href="" class="icon-video d-flex justify-content-center align-items-center"   data-toggle="modal" data-target="#md-video">
						<span class="fa fa-play"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="md-video" tabindex="-1" role="dialog" aria-labelledby="md-videoLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
                <h2 class="center"><?php echo $video['titulo'];?></h2>
				<iframe width="100%" height="400" src="https://www.youtube.com/embed/<?php echo $video['video'];?>">
				</iframe>
			</div>
		</div>
	</div>
</div>

<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(static/img/3.jpg);" data-stellabackground-ratio="0.5">
	<div class="container">
		<div class="row d-md-flex align-items-center justify-content-center">
			<div class="col-lg-10">
				<div class="row d-md-flex align-items-center">
					<div class="col-md d-flex justify-content-center countewrap ftco-animate">
						<div class="block-18">
							<div class="text">
								<strong class="number" data-number="<?php echo (date('Y')-1987);?>"><?php echo date('Y')-1987;?></strong>
								<span>Años de experiencia</span>
							</div>
						</div>
					</div>

					<div class="col-md d-flex justify-content-center countewrap ftco-animate">
						<div class="block-18">
							<div class="text">
								<span>Más de</span>
								<strong class="number" data-number="10000">10000</strong>
								<span>Clientes felices</span>
							</div>
						</div>
					</div>

					<div class="col-md d-flex justify-content-center countewrap ftco-animate">
						<div class="block-18">
							<div class="text">
								<strong class="number" data-number="26">26</strong>
								<span>Técnicos especializados</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

		

<section class="ftco-section testimony-section img" style="background-image: url(static/img/testimonios.jpg);">
	<div class="overlay"></div>

	<div class="container">
		<div class="row justify-content-center mb-5">
			<div class="col-md-7 text-center heading-section ftco-animate">
				<span class="subheading">Testimonios</span>
				<h2 class="mb-4">Clientes felices</h2>
			</div>
		</div>

		<div class="row ftco-animate justify-content-center">
			<div class="col-md-12">
				<div class="carousel-testimony owl-carousel ftco-owl">
					<div class="item">
						<div class="testimony-wrap text-center pb-5">
							<div class="user-img mb-4" style="background-image: url(static/img/usert.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="fa fa-quote-left"></i>
								</span>
							</div>

							<div class="text p-3">
								<p class="mb-4">La atención insuperable,saben hacer su trabajo, excelente.</p>
								<p class="name">Jesús Salvatierra</p>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="testimony-wrap text-center pb-5">
							<div class="user-img mb-4" style="background-image: url(static/img/usert.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="fa fa-quote-left"></i>
								</span>
							</div>

							<div class="text p-3">
								<p class="mb-4">RECOMENDADO, COMPRE MI CAMIONETA EN ESE CONCESIONARIO Y SIEMPRE LE HAGO SU SERVICIO EN DICHO LUGAR, EXCELENTE ATENCION, ESPACIOS COMODOS.</p>
								<p class="name">Hernan Machado</p>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="testimony-wrap text-center pb-5">
							<div class="user-img mb-4" style="background-image: url(static/img/usert.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="fa fa-quote-left"></i>
								</span>
							</div>

							<div class="text p-3">
								<p class="mb-4">La atención fue buena,tiene un lugar de espera con una computadora, TV, agua y WiFi.</p>
								<p class="name">Alcides Rodriguez</p>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="testimony-wrap text-center pb-5">
							<div class="user-img mb-4" style="background-image: url(static/img/usert.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="fa fa-quote-left"></i>
								</span>
							</div>

							<div class="text p-3">
								<p class="mb-4">Excelente servicio rápido y excelente taller.</p>
								<p class="name">Rigoberto Fernández</p>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="testimony-wrap text-center pb-5">
							<div class="user-img mb-4" style="background-image: url(static/img/usert.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="fa fa-quote-left"></i>
								</span>
							</div>

							<div class="text p-3">
								<p class="mb-4">Uso Toyota hace muchos años y siempre le hago el mantenimiento aquí. Son honestos y los costos son proporcionales a su calidad. Nada que objetar.</p>
								<p class="name">Joel Albornoz</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
