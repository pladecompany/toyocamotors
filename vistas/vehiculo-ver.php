<?php
  include_once("panel/modelo/Vehiculo.php");
  include_once("panel/modelo/Modelo.php");
  $veh = new Vehiculo();
  
  $id = $_GET['id'];
  $VEH = $veh->findById($id);
  if($VEH == false){
	echo "<script>window.location ='index.php';</script>";
	exit(1);
  }

  $mod = new Modelo();
  $modelo = $mod->findById($VEH['id_modelo']);
  $modelo = $modelo['modelo'];
?>

<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/1.jpg');" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate text-center">
				<h1 class="mb-2 bread">Vehículos</h1>
				<p class="breadcrumbs">
                  <span class="mr-2">
                    <a href="?op=inicio">Toyoca Motors <i class="ion-ios-arrow-forward"></i></a>
                  </span>
                  <span>Vehículos <i class="ion-ios-arrow-forward"></i></span>
                  <span><?php echo $VEH['nom_veh'];?></i></span>
                </p>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row m-0">
		<div class="col-sm-12">
			<section class="ftco-section">
				<div class="row">
					<div class="col-md-12 ftco-animate">
						<div class="blog-entry">

							<h3 class="heading text-center">
                                <?php echo $VEH['nom_veh'];?>
							</h3>

							<div class="row">
								<div class="col-xs-12 col-md-8 offset-md-2 p-0">
									<div class="sp-wrap">
										<a href="<?php echo $VEH['img1'];?>"><img src="<?php echo $VEH['img1'];?>" alt="" width="50px"></a>

                                        <?php if($VEH['img2'] != null) { ?>
                                          <a href="<?php echo $VEH['img2'];?>"><img src="<?php echo $VEH['img2'];?>" alt="" width="50px"></a>
                                        <?php } ?>

                                        <?php if($VEH['img3'] != null) { ?>
                                          <a href="<?php echo $VEH['img3'];?>"><img src="<?php echo $VEH['img3'];?>" alt="" width="50px"></a>
                                        <?php } ?>

                                        <?php if($VEH['img4'] != null) { ?>
                                          <a href="<?php echo $VEH['img4'];?>"><img src="<?php echo $VEH['img4'];?>" alt="" width="50px"></a>
                                        <?php } ?>

                                        <?php if($VEH['img5'] != null) { ?>
                                          <a href="<?php echo $VEH['img5'];?>"><img src="<?php echo $VEH['img5'];?>" alt="" width="50px"></a>
                                        <?php } ?>
									</div>
								</div>
							</div>

							<div class="row info_vehiculo mb-4 m-0">
								<div class="col-sm-3 text-center one">
									<i class="fa fa-car fa-2x"></i>
									<h6><?php echo strtoupper($modelo);?></h6>
								</div>
								
								<div class="col-sm-3 text-center two">
									<i class="fa fa-calendar fa-2x"></i>
									<h6><?php echo ($VEH['ano_veh']);?></h6>
								</div>
								
								<div class="col-sm-3 text-center three">
									<i class="fa fa-flag fa-2x"></i>
									<h6><?php echo ($VEH['imp_veh']==1)?'Importado':'Nacional';?></h6>
								</div>
								
								<div class="col-sm-3 text-center">
									<i class="fa fa-cog fa-2x"></i>
									<h6><?php echo $VEH['tra_veh'];?></h6>
								</div>
							</div>

                            <div class="row info_vehiculo mb-4 m-0">
                                <div class="col-xs-12 col-md-3 text-center">
                                    <i class="fas fa-coins fa-2x"></i>
                                    <h6>Precio  
                                      <span>
                                        <?php if($VEH['pre_veh'] == null || $VEH['pre_veh'] == 0) echo 'a consultar';else echo $VEH['pre_veh']." USD";?>
                                      </span>
                                    </h6>
                                </div>

                                <?php if($VEH['sto_veh'] == 1) { ?> 
								<div class="col-xs-12 col-md-6 text-center">
									<i class="fa fa-car fa-2x"></i>
                                    <h6>¿ Disponible en toyocamotors ? 
                                    <span><?php echo ($VEH['sto_veh']==1)?'Si':'No';?></span>
                                    </h6>
								</div>
                                <?php } ?>
                                 <?php if($VEH['doc'] != null) { ?> 
								<div class="col-xs-12 col-md-3 text-center">
									<i class="fa fa-file fa-2x"></i>
                                    <h6><a href='<?php echo ($VEH['doc']);?>' style="color:#fff;" target="__blank">Ficha técnica</a></h6>
								</div>
                                <?php } ?>
							</div>

							<div class="row">
								<div class="col-xs-12 col-md-4">
									<div class="list-group" id="list-tab" role="tablist">
										<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Descripción</a>
									
										<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Flexibilidad</a>
										
										<a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Diseño</a>
										
										<a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Seguridad</a>
									</div>
								</div>

								<div class="col-xs-12 col-md-8">
									<div class="tab-content" id="nav-tabContent">
										<div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
			                                <?php if(trim($VEH['des_veh']) != '') { ?>
												<div class="text_vehiculo">
													<h2 class="clr_red text-center">Descripción</h2>
													<p class="text-justify">
										              <?php echo nl2br($VEH['des_veh']);?>
			                                        </p>
												</div>
			                                <?php }?>
										</div>

										<div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
			                                <?php if(trim($VEH['fle_veh']) != '') { ?>
												<div class="text_vehiculo">
													<h2 class="clr_red text-center">Flexibilidad</h2>
													<p class="text-justify">
			                                          <?php echo nl2br($VEH['fle_veh']);?>
												</div>
			                                <?php }?>
										</div>

										<div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
			                                <?php if(trim($VEH['dis_veh']) != '') { ?>
												<div class="text_vehiculo">
													<h2 class="clr_red text-center">Diseño</h2>
													<p class="text-justify">
			                                          <?php echo nl2br($VEH['dis_veh']);?>
												</div>
			                                <?php }?>
										</div>

										<div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
			                                <?php if(trim($VEH['seg_veh']) != '') { ?>
												<div class="text_vehiculo">
													<h2 class="clr_red text-center">Seguridad</h2>
														<p class="text-justify">
			                                              <?php echo nl2br($VEH['seg_veh']);?>
												</div>
			                                <?php }?>
										</div>
									</div>
								</div>
							</div>

							<div class="text-center mt-3">
								<a class="btn btn-primary btn-lg" href="javascript:void(Tawk_API.toggle())"><i class="fa fa-car"></i> Me interesa</a>
								<br>
								<a href="?op=vehiculos"><i class="fa fa-angle-double-left"></i> Volver a vehículos</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>


<div class="modal fade" id="modalIngresar" tabindex="-1" role="dialog" aria-labelledby="modalIngresarLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalIngresarLabel">Iniciar sesión</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form class="form-a" method="POST" action="login.php">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="Cédula">Cédula</label>
							</div>
						</div>

						<div class="col-md-3" style="">
							<select class="form-control form-control-lg form-control-a" name="ced1" id="">
								<option selected="">V</option>
								<option>E</option>
								<option>J</option>
							</select>
						</div>

						<div class="col-md-9">
							<div class="form-group">
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu cédula" required name="ced2">
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
								<label for="Contraseña">Contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" name="pas" required minlength="5">
							</div>
						</div>
						<div class="col-md-12" id="" style="color:#000;">
						  <a href="#" id="bt_olvido" data-toggle="modal" data-target="#md-olvido">¿ Olvidó su contraseña ?</a>
						</div>
						<div class="col-md-12" id="msj_login" style="color:red;">
						</div>
					</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="btl">Ingresar</button>
                    </div>
				</form>
			</div>


		</div>
	</div>
</div>
