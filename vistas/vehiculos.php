<?php
   include_once("panel/modelo/Vehiculo.php");
   include_once("panel/modelo/Modelo.php");
?>
<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/vehiculos.jpg');" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate text-center icon_text">
				<h1 class="mb-2 bread">Vehículos</h1>
				<p class="breadcrumbs"><span class="mr-2"><a href="?op=inicio">Toyoca Motors <i class="ion-ios-arrow-forward"></i></a></span> <span>Vehículos <i class="ion-ios-arrow-forward"></i></span></p>
			</div>
		</div>
	</div>
</section>


<section class="ftco-section">
	<div class="container">
		<div class="container-fluid px-4">
			<div class="row">

          <?php
            $veh = new Vehiculo();
            if(isset($_GET['sto'])){
              $rv = $veh->fetchVehiculosActivosExiauto();
            }else if(isset($_GET['imp'])){
              $idm = $_GET['imp'];
              $rv = $veh->fetchVehiculosByImportado($idm);
            }else if(isset($_GET['cate'])){
              $idm = $_GET['cate'];
              $rv = $veh->fetchVehiculosByModelo($idm);
            }else{
              $rv = $veh->fetchVehiculosByImportado(0);
              echo "<div class='col-md-12'><h3>Nacionales</h3></div>";
              $OK = true;
            }

            while($fv = $rv->fetch_assoc()){
          ?>
                    <div class="col-md-6 col-lg-4 menu-wrap">
                        <div class="menus ftco-animate">
                            <img src="<?php echo $fv['img1'];?>" class=" menu-img img" width="100%">
                            <div class="text">
                                <div class=" p-2">
                                    <div class="d-flex">
                                        <div class="one-half" style="width: 70%;">
                                            <h6 class="title"><?php echo strtoupper($fv['nom_veh']);?></h6>
                                        </div>

                                        <div class="one-forth">
                                          <a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>"><span class="name">Ver más </span></a>
                                        </div>
                                    </div>

                                    <div class="one-half">
                                        <div class="row">
                                            <div class="info-vehiculos col-md-4 text-center icon_text">
                                                <i class="icon fa fa-calendar"></i>
                                                <h6><?php echo $fv['ano_veh'];?></h6>
                                            </div>

                                            <div class="info-vehiculos col-md-4 text-center icon_text">
                                                <i class="icon fa fa-flag"></i>
                                                <h6><?php echo ($fv['imp_veh']==1)?'Importado':'Nacional';?></h6>
                                            </div>

                                            <div class="info-vehiculos col-md-4 text-center icon_text">
                                                <i class="icon fa fa-cog"></i>
                                                <h6><?php echo $fv['tra_veh'];?></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="one-forth">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php 
                  }
                    if(isset($OK)){
                      $rv = $veh->fetchVehiculosByImportado(1);
                      echo "<div class='col-md-12'><h3>Importados</h3></div>";

                      while($fv = $rv->fetch_assoc()){
                 ?>
                    <div class="col-md-6 col-lg-4 menu-wrap">
                        <div class="menus ftco-animate">
                            <img src="<?php echo $fv['img1'];?>" class=" menu-img img" width="100%">
                            <div class="text">
                                <div class=" p-2">
                                    <div class="d-flex">
                                        <div class="one-half" style="width: 70%;">
                                            <h6 class="title"><?php echo strtoupper($fv['nom_veh']);?></h6>
                                        </div>

                                        <div class="one-forth">
                                          <a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>"><span class="name">Ver más </span></a>
                                        </div>
                                    </div>
                                    <div class="one-half">
                                        <div class="row">
                                            <div class="col-md-4 text-center icon_text">
                                                <i class="icon fa fa-calendar"></i>
                                                <h6><?php echo $fv['ano_veh'];?></h6>
                                            </div>

                                            <div class="col-md-4 text-center icon_text">
                                                <i class="icon fa fa-flag"></i>
                                                <h6><?php echo ($fv['imp_veh']==1)?'Importado':'Nacional';?></h6>
                                            </div>

                                            <div class="col-md-4 text-center icon_text">
                                                <i class="icon fa fa-cog"></i>
                                                <h6><?php echo $fv['tra_veh'];?></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="one-forth">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                      }
                  }else{
                    if($rv->num_rows == 0){
                      echo '<center><b>No hay resultados para el filtro seleccionado</b></center>';
                    }
                  }
                ?>
			</div>
		</div>
	</div>
</section>
