<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/repuestos.jpg');" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate text-center">
				<h1 class="mb-2 bread">Repuestos</h1>
				<p class="breadcrumbs"><span class="mr-2"><a href="?op=inicio">Toyoca Motors <i class="ion-ios-arrow-forward"></i></a></span> <span>Repuestos <i class="ion-ios-arrow-forward"></i></span></p>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="col-lg-8 offset-lg-2 pt-4 pb-4 ftco-animate fadeInUp ftco-animated">
		<p class="text-justify">Ofrecemos un excelente servicio y una profesional asesoría técnica a cada uno de nuestros clientes en cuanto a su auto.</p>

		<div class="row">
			<div class="col-xs-12 col-lg-8">
				<p class="text-justify">En <b>TOYOCA MOTORS</b> puede conseguir respuestos, para mantenerlo totalmente en su <b>línea original</b>, de alta calidad y durabilidad.</p>

				<p class="text-justify">Los repuestos están diseñados para darle <b>el máximo rendimiento y seguridad a tu TOYOTA</b>, nuestro inventario es diverso, acoplado a sus necesidades y capacidades</p>
			</div>

			<div class="col-xs-12 col-lg-4 text-center">
				<img src="static/img/car2.png" class="img-textos">
			</div>
		</div>

		<div class="text-center mb-5">
			<hr><h5>Sí necesitas preguntar o solicitar un repuesto para tu vehículo, tenemos para tí asesores de confianza Online</h5>
			<a class="btn btn-primary" href="javascript:void(Tawk_API.toggle())"> Preguntar / solicitar <b>repuesto</b></a>
		</div>
	</div>
</div>