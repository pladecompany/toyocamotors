

<div class="modal fade" id="modalRegistro" tabindex="-1" role="dialog" aria-labelledby="modalRegistroLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalRegistroLabel">Unirse a Toyoca Motors</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form class="form-a" action="panel/controlador/clientes.php" id="formulario_registro_cliente" method="POST">
					<div clas="col-md-12 text-center" style="display:none;text-align:center !important;" id="cont_msj">
					  <span id="txt_msj" style="color:red;text-align:center;"></span>
					  <br><br>
					</div>

					<div class="row">
						<div class="col-md-3 col-sm-3">
							<select class="form-control form-control-lg form-control-a" name="cod1" required id="tipo_per">
								<option selected="">V</option>
								<option>E</option>
								<option>J</option>
							</select>
						</div>

						<div class="col-md-9">
							<div class="form-group">
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Cédula/Rif" name="cod2" required minlength="3">
							</div>
						</div>

						<div class="col-md-12" style="display:none;" id="conte_razon">
							<div class="form-group">
								<label for="Correo">Razón social o empresa</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Razón social o empresa"  name="emp">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="Nombres">Nombres</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Nombres" required minlength="3" name="nom">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="Apellidos">Apellidos</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Apellidos" required minlength="3" name="ape">
							</div>
						</div>

						<div class="col-md-12">
							<label for="Teléfono">Teléfono de contacto</label>
						  <div class="row">
							<div class="form-group col-md-5">
								<select name="tlf1" class="form-control" required>
								  <option value="">--</option>
								  <option>0412</option>
								  <option>0424</option>
								  <option>0414</option>
								  <option>0416</option>
								  <option>0426</option>
								</select>
							</div>
							<div class="form-group col-md-7">
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Teléfono" name="tlf" required maxlength="7" minlength="7">
							</div>
						  </div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
								<label for="Correo">Correo</label>
								<input type="email" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo" required minlength="5" name="cor">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="Contraseña">Nueva contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" required minlength="5" name="pas">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="Contraseña">Confirmar contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Confirma tu contraseña" required minlength="5" name="cpa">
							</div>
						</div>


						<div class="col-md-12">
						  <small>*Nota: El télefono y correo facilitado seran utilizado por nuestra plataforma para enviarle información acerca de sus solicitudes de citas.</small>
						</div>
					</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" name="btg" class="btn btn-primary">Registrarse</button>
			</div>
				</form>
			</div>


		</div>
	</div>
</div>

<div class="modal fade" id="modalIngresar" tabindex="-1" role="dialog" aria-labelledby="modalIngresarLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalIngresarLabel">Iniciar sesión</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form class="form-a" method="POST" action="login.php">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="Cédula">Cédula</label>
							</div>
						</div>

						<div class="col-md-3" style="">
							<select class="form-control form-control-lg form-control-a" name="ced1" id="">
								<option selected="">V</option>
								<option>E</option>
								<option>J</option>
							</select>
						</div>

						<div class="col-md-9">
							<div class="form-group">
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu cédula" required name="ced2">
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
								<label for="Contraseña">Contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" name="pas" required minlength="5">
							</div>
						</div>
						<div class="col-md-12" id="" style="color:#000;">
						  <a href="#" id="bt_olvido" data-toggle="modal" data-target="#md-olvido">¿ Olvidó su contraseña ?</a>
						</div>
						<div class="col-md-12" id="msj_login" style="color:red;">
						</div>
					</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="btl">Ingresar</button>
                    </div>
				</form>
			</div>


		</div>
	</div>
</div>

<div id="md-olvido" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalIngresarLabel">¿ Olvidó su contrasaña ?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form class="form-a" method="POST" action="login.php">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="Cédula">Cédula</label>
							</div>
						</div>

						<div class="col-md-3" style="">
							<select class="form-control form-control-lg form-control-a" name="ced1" id="">
								<option selected="">V</option>
								<option>E</option>
								<option>J</option>
							</select>
						</div>

						<div class="col-md-9">
							<div class="form-group">
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu cédula" required name="ced2" value="<?php echo explode("-", $_GET['ced'])[1];?>">
							</div>
						</div>
						
						<?php
						if(isset($_GET['olvido']) && $_GET['paso'] == 2 && isset($_GET['txt'])){
						?>
						<div class="col-md-12">
							<div class="form-group">
								<label> Complete su correo electronico, para enviarle su contraseña de ingreso: <span style="color: red"><?php echo $_GET['txt'];?></span> </label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="<?php echo $_GET['txt'];?>" name="cor" required minlength="5">
							</div>
						</div>
						<?php }else if(isset($_GET['olvido']) && isset($_GET['no_aplica'])){ ?>
						<div class="col-md-12" style="color:red;">
						  La cédula ó el correo no coinciden.
						</div>
						<?php } ?>
					</div>
                    <div class="modal-footer">
                        <?php
                          if(isset($_GET['olvido'])&&$_GET['paso']==2&&isset($_GET['txt'])){
                            echo '<button type="submit" class="btn btn-primary" name="bt_clave">Enviar</button>';
                        }else{
                            echo '<button type="submit" class="btn btn-secondary" name="bto">Siguiente</button>';
                          } 
                        ?>
                    </div>
				</form>
			</div>


		</div>
	</div>
</div>

<div id="md-solicitud" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
					<h3 class="title-d">Realiza tus preguntas</h3>
				</div>
				<form class="form-a">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<label for="Desea saber acerca de:">Desea saber acerca de:</label>
								<select class="form-control form-control-lg form-control-a">
									<option selected="">Vehículos</option>
									<option>Repuestos</option>
									<option>Ambos</option>
								</select>
							</div>

							<div class="col-md-12">
								<label for="Mensaje">Mensaje</label>
								<textarea name="message" class="form-control" name="message" cols="45" rows="8" data-rule="required" data-msg="Escribe mínimo 3 carácteres" placeholder="Mensaje"></textarea>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-b">Enviar</button>
			</div>
		</div>
	</div>
</div>

<script>
  $(document).on('ready', function(){
	$("#tipo_per").on('change', function(){
	  var v = $("#tipo_per option:selected").val();
	  if(v != ""){
		if(v == "J")
		  $("#conte_razon").show();
		else
		  $("#conte_razon").hide();
	  }
	});
	$("#bt_olvido").on('click', function(){
	  $("#md-ingresar").modal("hide");
	  $("#md-olvido").modal("show");
	});
	$("#bt_registro_modal").on('click', function(){
	  $("#txt_msj").text("");
	});
  });
</script>
