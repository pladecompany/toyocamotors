<?php 
  include_once("panel/modelo/Noticia.php");
  $notit = new Noticia();
  $rnn = $notit->fetchAllActivas();
?>

<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/nosotros.jpg');" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate text-center">
				<h1 class="mb-2 bread">Noticias</h1>
				<p class="breadcrumbs"><span class="mr-2"><a href="?op=inicio">Toyoca Motors<i class="ion-ios-arrow-forward"></i></a></span> <span>Noticias <i class="ion-ios-arrow-forward"></i></span></p>
			</div>
		</div>
	</div>
</section>


<section class="ftco-section">
	<div class="container">
		<div class="row">

	        <?php
	          while($ffn = $rnn->fetch_assoc()){
                $feca = explode("-", $ffn['fec_reg_noti']);
	        ?>
			<div class="col-md-4 ftco-animate">
				<div class="blog-entry">
                    <img src="<?php echo $ffn['img'];?>" alt="" class="block-20 noticia">
					<div class="text p-3">
						<div class="meta">
							<div><a href="#" class="momento">Publicado el <?php echo $feca[2]." del mes ". $feca[1]." del ".$feca[0];?></a></div>
						</div>
						<h3 class="heading"><a href="?op=noticia-ver&id=<?php echo $ffn['id'];?>"><?php echo $ffn['titulo'];?></a></h3>
					    <h6>
                          <?php 
                            $des = substr($ffn['descripcion'], 0, 100);
                            echo $des;
                            if(strlen($ffn['descripcion']) > 100)
                              echo " . . .";
                          ?>

                        </h6>
						<p class="clearfix">
							<a href="?op=noticia-ver&id=<?php echo $ffn['id'];?>" class="float-left read">Leer más</a>
						</p>
					</div>
				</div>
			</div>
            <?php 
              }
            ?>
		</div>


	</div>
</section>
