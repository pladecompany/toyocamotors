<div class="header"></div>

<?php
	if(!isset($_SESSION['log'])){
		session_start();
		session_destroy();
		echo "<script>window.location ='index.php';</script>";
		exit(1);
	}

	include_once("panel/controlador/miscitas.php");
	include_once("panel/controlador/misvehiculos.php");
	include_once("panel/modelo/Cliente.php");

?>

	<?php if(!isset($_GET['calendario'])) { ?>
      <div class="container">
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h4 class="m-0 font-weight-bold color-b">Mis citas agendadas
              <br>
              <a class="" href="javascript:void(Tawk_API.toggle())" style="font-size:15px;"> ¿ Alguna duda sobre su cita ? Escribenos</a>
            </h4>
			<div class="text-right">
				<a href="#md-slider" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Nueva cita</b></a>
			</div>
		</div>
			
		<div class="card-body">
			<?php include_once("mensajes.php");?>
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>Enviado</th>
							<th>Vehículo</th>
							<th>Modelo</th>
							<th>Asesor</th>
							<th>Tipo de cita</th>
							<th>Cita</th>
							<th>Estatus</th>
							<th>Acciones</th>
						</tr>
					</thead>

					<tbody>
					<?php
						$noti = new Cita();
						$r = $noti->misCitas($_SESSION['idu']);
						$i=0;
						while($ff = $r->fetch_assoc()){
							$i++;
							echo "<tr>";
							echo "  <td>" . $i . "</td>";
							echo "  <td class='momento'>" . $ff['fec_env'] . "</td>";
							echo "  <td>" . $ff['placa'] . "</td>";
							echo "  <td>" . $ff['modelo'] . "</td>";
							echo "  <td>" . $ff['nom_age'] . " " .$ff['ape_age']."</td>";
							echo "  <td>" . $ff['tipo_cita'] . "</td>";
							echo "  <td class='momento' >" . $ff['fecha'] . "</td>";
							echo "  <td style='background: ".$noti->estatus($ff['estatus'])["color"].";color:#fff;'>" . $noti->estatus($ff['estatus'])["txt"] . "</td>";
							echo "<td class='text-center'>";
							if($ff['estatus'] == 0 || $ff['estatus'] == 1){
								echo "<a href='?op=cita&el=".$ff['idc']."' title='Cancelar' onclick='return confirm(\"¿ Esta seguro que desea cancelar la cita ?\")'><i class='mr-2 fa fa-times'></i></a>";
							}
							echo "<a href='#' title='Ver detalles' class='bt_detalle' id='".$ff['idc']."'><i class='mr-2 fa fa-eye'></i></a>";
							echo "</td>";
							echo "</tr>";
						}
										?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div id="md-slider" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="title-d" id="titulo_modulo">Solicitar cita</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<form class="form-a" method="GET" action="" enctype="multipart/form-data" id="">
						<input type="hidden" name="op" value="cita">
						<input type="hidden" name="calendario" value="1">
						<?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
						<div class="row">
							<div class="col-md-6 mb-2">
								<div class="form-group">
									<label for="Modelo">Seleccione vehículo</label>
									<select name="veh" class="form-control" required>
											<option value="">Seleccione</option>
									<?php
										$mod = new MiVehiculo();
										$r = $mod->misVehiculos($_SESSION['idu']);
										while($fm = $r->fetch_assoc()){
											if(isset($_GET['idv']) && $_GET['idv'] == $fm['idv']){
									?>
											<option value="<?php echo $fm['idv'];?>" selected>(<?php echo strtoupper($fm['placa']);?>) - <?php echo $fm['modelo'] . " - " . $fm['serial1'];?> </option>
									<?php }else{ ?>
											<option value="<?php echo $fm['idv'];?>" >(<?php echo strtoupper($fm['placa']);?>) - <?php echo $fm['modelo'] . " - " . $fm['serial1'];?> </option>
									<?php
												}
										}
									?>
								</select>
                                <?php if($r->num_rows ==0) echo "<a href=''>No tiene ningún vehículo registrado, click para registrar uno</a>"; ?>
								</div>
							</div>
							<div class="col-md-6 mb-2">
								<div class="form-group">
									<label for="Tipo">Seleccione tipo de cita</label>
									<select name="tip" id="tipo_cita_cita" class="form-control" required>
                                      <option value="">Seleccione</option>
                                      <?php
                                      $cit = new Cita();
                                    $r = $cit->tiposCitas();
                                    while($fm = $r->fetch_assoc()){
                                      ?>
                                        <option value="<?php echo $fm['id'];?>" hab="<?php echo $fm['habilita'];?>"><?php echo $fm['tipo_cita'];?></option>
                                        <?php
                                    }
                                    ?>
									</select>
								</div>
							</div>

							<div class="col-md-6 mb-2" style="display:none;">
								<div class="form-group">
									<label for="Modelo">Seleccione revisión</label>
										<select name="rev" class="form-control" >
												<option value="" selected>Seleccione</option>
										<?php
											$mod = new MiVehiculo();
											$r = $mod->fallas();
											while($fm = $r->fetch_assoc()){
										?>
												<option value="<?php echo $fm['id'];?>" ><?php echo strtoupper($fm['falla']);?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>

							<div class="col-md-6 mb-2">
								<div class="form-group">
									<label for="Modelo">Kilometraje actual</label>
									<input type="number" class="form-control" name="kil" min="1000" max="500000" style="height:3.5rem;" required>
								</div>
							</div>

							<div class="col-md-12 mb-2" id="cont_motivo_cita">
								<div class="form-group">
									<label for="Modelo">¿ Motivo de la cita ?</label>
									<textarea name="des" class="form-control form-control-lg form-control-a" style="height:100px;"></textarea>
								</div>
							</div>

							<div class="col-md-12 text-center">
								<label for="Modelo">Seleccione el asesor de trabajo</label>
								<div class="row">
								<?php
									$mod = new Agente();
									$r = $mod->fetchAll();
									while($fm = $r->fetch_assoc()){
                                      if($fm['est_age'] == 1){
								?>
								<div class="col-md-3 text-center">
									<div class="text-center">
										<div class="col-md-12">
											  <label for="radio_<?php echo $fm['id'];?>"><img src="<?php echo $fm['img_age'];?>" class="img-user rounded-circle" style="cursor:pointer;"></label>
                                        </div>
										<b><?php echo $fm['cod_age'];?></b><br>
										<?php echo strtoupper($fm['nom_age'] . " " . $fm['ape_age']);?>
										<input type="radio" id="radio_<?php echo $fm['id'];?>" name="age" value="<?php echo $fm['id'];?>" required>
										<br>
									</div>
									<hr>
								</div>
								<?php
                                  }
									}
								?>
								</div>
						</div>
							<div class="col-md-12 text-center">
							<?php 
								$usu = new Cliente();
								$fu = $usu->findById($_SESSION['idu']);
							?>
								<label>Para cualquier información extra que necesitemos, te contactaremos a: <br><a href="?op=perfil" style="color:red;"><?php echo $fu['tel_usu']. " - ".$fu['cor_usu'];?></a></label>
							</div>

							</div>

							<div class="modal-footer">
									<button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-primary"><?php echo ((isset($F))?'Siguiente':'Siguiente')?></button>
							</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div id="md-cita-detalles" class="modal modalmedium fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
									<h5>Detalles de la solicitud</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="title-box-d">
						<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="">
								<div class="row">
										<div class="col-md-3 ">
												<div class="form-group">
														<label for="Modelo"><b>Estatus </b></label>
												</div>
										</div>
										<div class="col-md-9 ">
												<div class="form-group text-center" id="estatus" style="color:#fff;">
												</div>
										</div>
										<div class="col-md-3 ">
												<div class="form-group">
														<label for="Modelo"><b>Enviada</b></label>
												</div>
										</div>
										<div class="col-md-9 ">
												<div class="form-group momento" id="enviado">
												</div>
										</div>
										<div class="col-md-3 ">
												<div class="form-group">
														<label for="Modelo"><b>Cita</b></label>
												</div>
										</div>
										<div class="col-md-9 ">
												<div class="form-group momento" id="fec_cita">
												</div>
										</div>
										<div class="col-md-3 ">
												<div class="form-group">
														<label for="Modelo"><b>Asesor</b></label>
												</div>
										</div>
										<div class="col-md-9 ">
												<div class="form-group" id="agente">
												</div>
										</div>
										<div class="col-md-3 ">
												<div class="form-group">
														<label for="Modelo"><b>Vehículo</b></label>
												</div>
										</div>
										<div class="col-md-9 ">
												<div class="form-group" id="vehiculo">
												</div>
										</div>
										<div class="col-md-3 ">
												<div class="form-group">
														<label for="Modelo"><b>Kilometros </b></label>
												</div>
										</div>
										<div class="col-md-9 ">
												<div class="form-group" id="kilometros">
												</div>
										</div>
										<div class="col-md-3 " style="display:none;">
												<div class="form-group">
														<label for="Modelo"><b>Falla </b></label>
												</div>
										</div>
										<div class="col-md-9 ">
												<div class="form-group" id="falla">
												</div>
										</div>
										<div class="col-md-12 ">
												<div class="form-group">
														<label for="Modelo"><b>Tipo y motivo de la cita</b></label>
												</div>
										</div>
										<div class="col-md-12 ">
												<div class="form-group" id="motivo">
												</div>
										</div>
										<div class="col-md-12 " style="" id="conte_observacion">
												<label for="Modelo"><b>Observación enviada por la administración:</b></label>
												<div class="form-group">
													<div class="form-group" id="observacion">
												</div>
										</div>
										<div class="col-md-12 mb-2">
												<div style="display:none;" class="text-center alert alert-info" id="mensajes"></div>
												<div style="display:none;" class="img_cargando" id="img_cargando"><img src="static/img/cargando.gif" style="width:50px;"></div>
										</div>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php }

	if(isset($_GET['calendario'])){
		include_once("calendario.php");
	}
?>


<?php
	if(isset($_GET['solicitar'])){
?>
	<script>
		$(document).ready(function(){
			$("#bt_nueva_noticia").trigger('click');
		});
	</script>

<?php
	} 
?>
<script>
		$(document).ready(function(){
            $("#tipo_cita_cita").change(function(){
              var hab = $("#tipo_cita_cita option:selected").attr('hab');
              if(hab == 0) $("#cont_motivo_cita").slideUp('slow');
              else $("#cont_motivo_cita").slideDown('slow');
            });

			$("#bt_nueva_noticia").click(function(){
				$("#titulo_modulo").text("Agendar cita");
				$("#bt_modulo").attr('name', 'btg');
				$("#bt_modulo").text('Siguiente');
				$("input[name='pla']").val('');
				$("input[name='ser1']").val('');
				$("input[name='ser2']").val('');
				$("input[name='ser3']").val('');
				$("input[name='ser4']").val('');
				$("input[name='ano']").val('');
				$("select[name='mod']").val('');
			});

			moment.locale('es');         // en
			$(".momento").each(function(){
				$(this).text(moment($(this).text()).format('llll'));
			});

			<?php if(isset($_GET['nuevacita'])){
			?>
				$("#md-slider").modal("show");
			<?php
			}
			?>
			$(document).on('click', '.bt_detalle', function(){
				var idd = this.id;
				$("#md-cita-detalles").modal("show");
				$(".img_cargando").show();
				$.post('panel/ajax_php.php', {modulo: 'citas', tipo: 'obtenerCita', idc: idd}, function(data){
					$(".img_cargando").hide();
					if(data.r == false) {
						alert(data.msj);
						return;
					}
					var dataCita = data.cita;
							$("#estatus").text(dataCita.txt).css('background', dataCita.color);
							$("#fec_cita").text(dataCita.fecha)
							$("#agente").text("("+dataCita.cod_age+") " + dataCita.nom_age + " " + dataCita.ape_age);
							$("#vehiculo").text("("+dataCita.placa+") " + dataCita.serial1 + " AÑO: " + dataCita.ano + " - " + dataCita.modelo);
							$("#enviado").text(dataCita.fec_env);
							$("#kilometros").text(dataCita.kilometros);
							//$("#falla").text(dataCita.falla);
							$("#observacion").text(dataCita.observacion);
							$("#motivo").text(dataCita.tipo_cita+": " + dataCita.motivo);
							$("#md-cita").modal('show');
							$("#mensajes").hide();
							$(".btop").each(function(){
								$(this).attr('id_cita', dataCita.idc);
							});
							$(".momento").each(function(){
								$(this).text(moment($(this).text()).format('llll'));
							});
							$(".btop").show();
							if(dataCita.estatus == 0){
							}else if(dataCita.estatus == 1)
								$("#bt_aprobar").hide();
							else if(dataCita.estatus == -1)
								$("#bt_cancelar").hide();
							else if(dataCita.estatus == -2)
								$("#bt_rechazar").hide();
							else if(dataCita.estatus == 2){
								$("#bt_finalizar").hide();
								$("#bt_aprobar").hide();
							}
				});
			});
		});

</script>

