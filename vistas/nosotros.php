<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/nosotros.jpg');" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-end justify-content-center">
			<div class="col-md-9 ftco-animate text-center">
				<h1 class="mb-2 bread">Nosotros</h1>
				<p class="breadcrumbs"><span class="mr-2"><a href="?op=inicio">Toyoca Motors <i class="ion-ios-arrow-forward"></i></a></span> <span>Nosotros <i class="ion-ios-arrow-forward"></i></span></p>
			</div>
		</div>
	</div>
</section>


<section class="ftco-section ftco-wrap-about ftco-no-pb">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-10 wrap-about ftco-animate text-center">
				<div class="heading-section mb-4 text-center">
					<span class="subheading">Nosotros</span>
					<h2 class="mb-4">Toyoca Motors</h2>
				</div>
				<p>Somos un equipo especializado para el diagnóstico de tu Vehículo, donde puedes solicitar tu cita Online y llevar el control de tus servicios. Toyota Motors te brinda el servicio Toyota, latonería, pintura para tú vehículo y repuestos originales para mantenenerlo con calidad y durabilidad.</p>

				<div class="video justify-content-center">
					<a href="https://vimeo.com/45830194" class="icon-video popup-vimeo d-flex justify-content-center align-items-center">
						<span class="ion-ios-play"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(static/img/4.jpg);" data-stellar-background-ratio="0.5">
	<div class="container">
		<div class="row d-md-flex align-items-center justify-content-center">
			<div class="col-lg-10">
				<div class="row d-md-flex align-items-center">
					<div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
						<div class="block-18">
							<div class="text">
								<strong class="number" data-number="<?php echo (date('Y')-1987);?>"><?php echo (date('Y')-1987);?></strong>
								<span>Años de experiencia</span>
							</div>
						</div>
					</div>

					<div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
						<div class="block-18">
							<div class="text">
								<span>Más de</span>
								<strong class="number" data-number="10000">10000</strong>
								<span>Clientes felices</span>
							</div>
						</div>
					</div>

					<div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
						<div class="block-18">
							<div class="text">
								<strong class="number" data-number="26">26</strong>
								<span>Técnicos especializados</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="ftco-section bg-light">
	<div class="container">
		<div class="row justify-content-center mb-5 pb-2">
			<div class="col-md-7 text-center heading-section ftco-animate">
			<span class="subheading">Beneficios</span>
			<h2 class="mb-4">Todo para tu Toyota</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
				<div class="media block-6 services d-block">
					<div class="icon d-flex justify-content-center align-items-center">
						<img src="static/img/icon-1.png" width="60%">
					</div>

					<div class="media-body p-2 mt-3">
						<h3 class="heading">Servicio</h3>
						<p>Ofrecemos el mantenimiento periódico preventivo de tu vehículo, <br>solicita tu <a href="">Cita online</a></p>
						<a href="?op=servicio">Saber más</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
				<div class="media block-6 services d-block">
					<div class="icon d-flex justify-content-center align-items-center">
						<img src="static/img/icon-2.png" width="70%">
					</div>

					<div class="media-body p-2 mt-3">
						<h3 class="heading">Latonería y pintura</h3>
						<p>Contamos con un equipo de técnicos especializados, <br>solicita tu <a href="">Cita online</a></p>
						<a href="?op=latoneria">Saber más</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
				<div class="media block-6 services d-block">
					<div class="icon d-flex justify-content-center align-items-center">
						<img src="static/img/icon-3.png" width="75%">
					</div>

					<div class="media-body p-2 mt-3">
						<h3 class="heading">Repuestos y accesorios</h3>
						<p>Para que su Toyota se conserve siempre original tenemos para usted variedad en repuestos y accesorios originales</p>
						<a href="?op=repuestos">Saber más</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
