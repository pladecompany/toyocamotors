<?php
  if(!isset($_SESSION['log'])){
    session_start();
    session_destroy();
    echo "<script>window.location ='index.php';</script>";
    exit(1);
  }


?>

<div class="header"></div>

<div class="container">
	<div class="row mt-5 mb-5 pt-5 pb-5">
		<a href="?op=misvehiculos" class="col-xl-6 col-md-3 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Mis vehículos</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-car fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>
		
		<a href="?op=perfil" class="col-xl-6 col-md-3 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Perfil</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-user fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="?op=cita" class="col-xl-6 col-md-3 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Mis citas</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-list fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="salir.php" class="col-xl-6 col-md-3 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Salir</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-lock fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>
