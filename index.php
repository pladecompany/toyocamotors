<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
    session_start();
	include_once('ruta.php');
    include_once("configuraciones.php");

    /*
    if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
        $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $location);
        exit;
    } */
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Toyoca Motors  C.A. – Concesionario Toyota.</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-toto-fit=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="title" content="Toyoca Motors  C.A. – Concesionario Toyota.">
	<meta name="author" content="Plade Company C.A">
	<meta name="keywords" content="Toyoca Motors, Concesionario, Toyota, Vehículos, carros, compra, ventas">
	<meta name="description" content="Especializados en el diagnóstico de tu Vehículo. Te brindamos el servicio de latonería, pintura y repuestos originales. Solicita tu cita Online.">
	<link rel="icon" href="static/img/ToyocaMotors.png">
    <link rel="stylesheet" href="static/css/animate.css">
    <link rel="stylesheet" href="static/css/owl.carousel.min.css">
    <link rel="stylesheet" href="static/css/magnific-popup.css">
    <link rel="stylesheet" href="static/css/style.css">
	<link rel="stylesheet" href="static/css/all.min.css">
	<link rel="stylesheet" href="static/css/smoothproducts.css">
	<script src="static/js/jquery.min.js"></script>
    <script src='fullcalendar/packages/moment.js'></script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5dd7efd143be710e1d1ea534/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    <?php include_once("gtag.php");?>
</head>

<body>
	<div class="py-1 bg-black top">
		<div class="container">
			<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
				<div class="col-lg-12 d-block">
					<div class="row d-flex">
						<div class="col-md pr-4 d-flex topper align-items-center">
							<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
							<span class="text">Master 212-239-20-24</span>
						</div>

						<div class="col-md pr-4 d-flex topper align-items-center">
							<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
							<span class="text">info@toyocamotors.com</span>
						</div>

						<div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right justify-content-end">
							<p class="mb-0 register-link"><span>Horarios:</span> <span>Lunes - Viernes</span> <span>7:30AM - 5:00PM</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
		<div class="container">
			<a class="navbar-brand" href="?op=inicio"><img src="static/img/ToyocaMotors.png" alt="Logo de Toyoca Motors"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>

			<div class="collapse navbar-collapse" id="ftco-nav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a href="?op=inicio" class="nav-link">Inicio</a></li>
					<li class="nav-item"><a href="?op=nosotros" class="nav-link">Nosotros</a></li>
					<li class="nav-item"><a href="?op=vehiculos" class="nav-link">Vehículos</a></li>
					<li class="nav-item"><a href="?op=servicio" class="nav-link">Servicio</a></li>
					<li class="nav-item"><a href="?op=latoneria" class="nav-link">Latonería y pintura</a></li>
					<li class="nav-item"><a href="?op=repuestos" class="nav-link">Repuestos</a></li>
					<li class="nav-item"><a href="?op=noticias" class="nav-link">Noticias</a></li>
					<li class="nav-item"><a href="?op=contacto" class="nav-link">Contacto</a></li>
                    <?php
                      if(isset($_SESSION['log']) && $_SESSION['log'] == true){
                      }else{
                    ?>
                      <li class="nav-item"><a href="#" class="nav-link"  data-toggle="modal" data-target="#modalIngresar" id="bt_ingresar">Ingresar</a></li>
                      <?php }?>
                    <?php
                      if(isset($_SESSION['log'])&&$_SESSION['log'] == true){
                    ?>
                      <li class="nav-item cta"><a href="?op=inicio_log" class=" btn btn-primary"> <?php echo "Hola ". $_SESSION['nom'];?> </a></li>
                    <?php
                      }else{
                    ?>
                      <li class="nav-item cta"><a href="#" class="btn btn-primary"  data-toggle="modal" data-target="#modalRegistro" id="bt_registro_modal"><i class="fa fa-user"></i> Unirse</a></li>
                    <?php }?>


                    <?php
                      if($promo['est'] == "1"){
                    ?>
                      <li class="nav-item">
                          <li class="nav-item cta"><a href="#" class="btn btn-primary"  data-toggle="modal" data-target="#md-promo" id="bt_promo"><i class="fa fa-star"></i> Promo</a></li>
                      </li>
                    <?php } ?>
				</ul>
			</div>
		</div>
	</nav>

	<div>
		<?php include_once($ruta); ?>
		<?php include_once("vistas/modales.php"); ?>
	</div>
    <?php if($_GET['op'] == 'inicio') { ?>
	<section class="ftco-section ftco-no-pt ftco-no-pb">
		<div class="container-fluid px-0">
			<div class="row no-gutters">
				<div class="col-md">
					<a href="#" class="instagram img d-flex align-items-center justify-content-center" style="background-image: url(static/img/9.jpg);">
						<!--<span class="ion-logo-instagram"></span>-->
					</a>
				</div>
				<div class="col-md">
					<a href="#" class="instagram img d-flex align-items-center justify-content-center" style="background-image: url(static/img/2.jpg);">
						<!--<span class="ion-logo-instagram"></span>-->
					</a>
				</div>
				<div class="col-md">
					<a href="#" class="instagram img d-flex align-items-center justify-content-center" style="background-image: url(static/img/4.jpg);">
						<!--<span class="ion-logo-instagram"></span>-->
					</a>
				</div>
				<div class="col-md">
					<a href="#" class="instagram img d-flex align-items-center justify-content-center" style="background-image: url(static/img/contacto.jpg);">
						<!--<span class="ion-logo-instagram"></span>-->
					</a>
				</div>
				<div class="col-md">
					<a href="#" class="instagram img d-flex align-items-center justify-content-center" style="background-image: url(static/img/5.jpg);">
						<!--<span class="ion-logo-instagram"></span>-->
					</a>
				</div>
			</div>
		</div>
	</section>
    <?php } ?>

	<footer class="ftco-footer ftco-bg-dark ftco-section">
		<div class="container-fluid px-md-5 px-3">
			<div class="row mb-5">
				<div class="col-md-12 col-lg-4">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Toyoca Motors C.A</h2>
						<p>Somos un equipo especializado para el diagnóstico de tu Vehículo, desde nuestras cómodas instalaciones brindamos el servicio Toyota y repuestos originales para mantenenerlo con calidad y durabilidad.</p>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Ubícanos</h2>
						<ul class="list-unstyled open-hours">
							<li class="d-flex"><span>Av. Principal de los Ruices. Edificio Stemo. PB, locales 1,2,3. Caracas - Venezuela</span></li>
							<li class="d-flex"><span>Lunes a viernes, 7:30AM - 5:00PM</span></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Síguenos</h2>
						<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
							<li class="ftco-animate"><a href="#" target="_blank"><span class="fab fa-twitter"></span></a></li>
							<li class="ftco-animate"><a href="#" target="_blank"><span class="fab fa-facebook-f"></span></a></li>
							<li class="ftco-animate"><a href="#" target="_blank"><span class="fab fa-instagram"></span></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 text-center">
					<p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Desarrollado por <a href="https://pladecompany.com" target="_blank">Plade Company</a></p>
				</div>
			</div>
		</div>
	</footer>


<?php
  include_once("promo.php");
?>
  <!-- loader 
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
-->

	<script src="static/js/jquery-migrate-3.0.1.min.js"></script>
	<script src="static/js/popper.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
	<script src="static/js/jquery.easing.1.3.js"></script>
	<script src="static/js/jquery.waypoints.min.js"></script>
	<script src="static/js/jquery.stellar.min.js"></script>
	<script src="static/js/owl.carousel.min.js"></script>
	<script src="static/js/jquery.magnific-popup.min.js"></script>
	<script src="static/js/aos.js"></script>
	<script src="static/js/bootstrap-datepicker.js"></script>
	<script src="static/js/jquery.timepicker.min.js"></script>
	<script src="static/js/jquery.animateNumber.min.js"></script>
	<script src="static/js/scrollax.min.js"></script>
	<script src="static/js/main.js"></script>
	<script src="static/js/registro.js"></script>
	
	<script type="text/javascript" src="static/js/smoothproducts.min.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			$('.sp-wrap').smoothproducts();
		});

	</script>

	<script type="text/javascript">
		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').trigger('focus')
		})

        <?php
          if(isset($_GET['reg'])){
        ?>
            $("#bt_registro_modal").trigger('click');
        <?php
            if(isset($_GET['msj'])){
        ?>
            $("#cont_msj").show();
            $("#txt_msj").text("** <?php echo $_GET['msj'];?> **");
        <?php
            }
          }

          if($promo['est'] == "1"){ // Si la promocion es visible desde el panel admin entra acá
            // Validamos la session, si existe la sesion no mostramos modal, cuando es primera vez si lo mostramos.
            if( isset($_SESSION['ok']) ){
            }else{
              $_SESSION['ok'] = true;
        ?>
            //$('#md-promo').modal('show');
            $("#bt_promo").trigger('click');
            //$('#video_modal').trigger('play');
        <?php
            }
          }
        ?>


        $("#bt_promo").on('click', function(){
            $('#md-promo').modal('show');
        });

      <?php
        if(isset($_GET['clave'])){
      ?>
        $("#bt_ingresar").trigger('click');
          $('#msj_login').text('Su contraseña de ingreso se envio a su correo, verifiquela y vuelva ingresar');
      <?php
        }
      ?>

      $(document).on('click', '#bt_reg_log', function(){
        $("#md-ingresar").modal("hide");
        $("#bt_registro_modal").trigger('click');
      });

      <?php
        if(isset($_GET['err']) && !isset($_GET['reg'])){
          ?>
          $("#bt_ingresar").trigger('click');
          var html = "Datos invalidos, desea registrarse ? <input type='button' class='btn btn-success' value='Registrarse' id='bt_reg_log'>";
          $("#msj_login").html(html);
          <?php
        }
      ?>
      <?php
        if(isset($_GET['olvido'])){
          ?>
          $("#md-olvido").modal("show");
          <?php
        }
      ?>


	</script>
  </body>
</html>
